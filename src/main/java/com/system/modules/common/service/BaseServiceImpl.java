package com.system.modules.common.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.system.modules.common.dao.BaseDao;
import com.system.modules.common.model.BaseEntity;

public class BaseServiceImpl implements BaseService {

	@Autowired
	private BaseDao baseDao;

	@Override
	public void saveBaseEntity(BaseEntity entity) {
		if(entity.getId() == null)
		entity.setCreationDate(new Date());
		getBaseDao().save(entity);
	}

	@Override
	public <T> void saveOrUpdate(T entity) {
		if (entity instanceof BaseEntity && ((BaseEntity) entity).getId() == null) {
			((BaseEntity) entity).setCreationDate(new Date());
		}
		getBaseDao().saveOrUpdate(entity);
	}

	@Override
	public <T> void deleteEntity(T entity) {
		getBaseDao().deleteEntity(entity);
	}

	@Override
	public <T> void update(T entity) {
		getBaseDao().update(entity);
	}

	@Override
	public void clear() {
		getBaseDao().clear();
	}

	@Override
	public void flush() {
		getBaseDao().flush();
	}

	@Override
	public void deleteById(Long id, Class<?> clazz) {
		getBaseDao().deleteById(id, clazz);
	}

	@Override
	public void deleteAll(Class<?> clazz) {
		getBaseDao().deleteAll(clazz);
	}

	@Override
	public Class<?> loadById(Long id, Class<?> clazz) {
		return getBaseDao().loadById(id, clazz);
	}

	@Override
	public <T extends BaseEntity> List<T> getAll(Class<T> calzz) {
		return getBaseDao().getAll(calzz);
	}

	@Override
	public <T extends BaseEntity> T getObjectById(Long id, Class<T> clazz) {
		return getBaseDao().getObjectById(id, clazz);
	}

	@Override
	public <T> void persist(T entity) {
		getBaseDao().persist(entity);
	}

	public BaseDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao baseDao) {
		this.baseDao = baseDao;
	}
}
