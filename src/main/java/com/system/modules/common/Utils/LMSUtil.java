package com.system.modules.common.Utils;

import org.joda.time.DateTime;

public final class LMSUtil {
	
	public static DateTime getTermStart(){
		// term start at next week
    	DateTime termStart = new DateTime().plusDays(6 - new DateTime().getDayOfWeek());
		return termStart;
	}
	
	public static DateTime getTermEnd(){
		// term end at next week plus one month
		DateTime termEnd = new DateTime().plusDays(6 - new DateTime().getDayOfWeek()).plusMonths(1);
    	return termEnd;
	}
}
