package com.system.modules.common.dao;

import java.util.List;

import org.hibernate.Session;

import com.system.modules.common.model.BaseEntity;

public interface BaseDao {

	<T> void saveOrUpdate(T entity);

	<T> void deleteEntity(T entity);

	<T> void update(T entity);

	void clear();

	void flush();

	void deleteById(Long id, Class<?> clazz);


	void deleteAll(Class<?> clazz);

	Class<?> loadById(Long id, Class<?> clazz);

	<T extends BaseEntity> List<T> getAll(Class<T> calzz);

	<T extends BaseEntity> T getObjectById(Long id, Class<T> clazz);

	<T> void persist(T entity);

	void save(BaseEntity entity);
	
	public Long getMaxId(Class<?> clazz);

	Session getSession();
}
