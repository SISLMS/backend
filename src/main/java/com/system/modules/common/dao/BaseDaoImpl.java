package com.system.modules.common.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.system.modules.common.model.BaseEntity;

@Repository("baseDao")
public class BaseDaoImpl implements BaseDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void save(BaseEntity entity) {
//		entity.setCreatedBy(loggedInUser);
		entity.setCreationDate(new Date());
		getSession().save(entity);
	}

	@Override
	public <T> void saveOrUpdate(T entity) {
		getSession().saveOrUpdate(entity);
	}
	
	@Override
	public <O> void persist(O entity) {
		getSession().persist(entity);
	}
	
	@Override
	public <T> void deleteEntity(T entity) {
		getSession().delete(entity);
	}

	@Override
	public  void deleteAll(Class<?> entity) {
		String sql = "DELETE FROM " + entity.getClass().getName();
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.executeUpdate();
	}

	@Override
	public <T> void update(T entity) {
		getSession().update(entity);
	}

	@Override
	public <T extends BaseEntity> T getObjectById(Long id, Class<T> clazz) {
		return (T) getSession().get(clazz, id);
	}

	@Override
	public <T extends BaseEntity> List<T> getAll(Class<T> calzz) {
		Criteria crit = getSession().createCriteria(calzz);
		return crit.list();
	}

	@Override
	public void deleteById(Long id, Class<?> clazz) {
		// String sql = "delete from "+entityClass.getCanonicalName().replace("com.system.modules.core.model.", "")+" where id="+id;
		Table table = clazz.getClass().getAnnotation(Table.class);
		table.name();
		String tableName = (table.name() != null || !table.name().isEmpty()) ? table.name()
				: clazz.getSimpleName();
		String sql = "delete from " + tableName + "  where id=" + id;
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.executeUpdate();
	}
	
	@Override
	public Long getMaxId(Class<?> clazz) {
		Criteria criteria = getSession().createCriteria(clazz).setProjection(Projections.max("id"));
		Long maxId = (Long)criteria.uniqueResult();
		return maxId;
	}
	
	@Override
	public Class<?> loadById(Long id, Class<?> clazz) {
		return (Class<?>) getSession().load(clazz, id);
	}
	
	@Override
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void clear() {
		getSession().clear();
	}

	@Override
	public void flush() {
		getSession().flush();
	}

}
