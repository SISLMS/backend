package com.system.modules.common.dto;

import java.util.ArrayList;
import java.util.List;

public class JsTreeDTO {
	private String entityId;
	private String entityParentId;
	private String entityTypeName;
	private String text = "";
	private String parent = "";
	private State state;
	private List<JsTreeDTO> children = new ArrayList<JsTreeDTO>();

	static public class State {
		private Long id;
		private Boolean selected;
		private Boolean disable;
		private Boolean opened;

		public Boolean getDisable() {
			return disable;
		}

		public void setDisable(Boolean disable) {
			this.disable = disable;
		}

		public Boolean getSelected() {
			return (selected != null) ? selected : false;
		}

		public void setSelected(Boolean selected) {
			this.selected = selected;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Boolean getOpened() {
			return opened;
		}

		public void setOpened(Boolean opened) {
			this.opened = opened;
		}
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public List<JsTreeDTO> getChildren() {
		return children;
	}

	public void setChildren(List<JsTreeDTO> children) {
		this.children = children;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntityTypeName() {
		return entityTypeName;
	}

	public void setEntityTypeName(String entityTypeName) {
		this.entityTypeName = entityTypeName;
	}

	public String getEntityParentId() {
		return entityParentId;
	}

	public void setEntityParentId(String entityParentId) {
		this.entityParentId = entityParentId;
	}

}
