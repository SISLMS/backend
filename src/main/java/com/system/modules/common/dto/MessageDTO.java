package com.system.modules.common.dto;

public class MessageDTO {

	private String messageCode;
	private String messageText;

	public MessageDTO(String messageCode, String messageText) {
		this.messageCode = messageCode;
		this.messageText = messageText;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

}
