package com.system.modules.common.exception;

import java.util.List;

import com.system.modules.common.exception.enums.ValidationsEnum;

public class DBValidationException extends RuntimeException {
	private static final long serialVersionUID = -702764473852985983L;
	
	private ValidationsEnum validationsEnum;
	private List<ValidationsEnum> validationsEnums;
	
	
	
	public DBValidationException(ValidationsEnum dbValidationEnum){
		this.setValidationsEnum(dbValidationEnum);
	}
	public DBValidationException(List<ValidationsEnum> dBValidationEnums) {
		this.setValidationsEnums(dBValidationEnums);
	}
	public ValidationsEnum getValidationsEnum() {
		return validationsEnum;
	}
	public void setValidationsEnum(ValidationsEnum validationsEnum) {
		this.validationsEnum = validationsEnum;
	}
	public List<ValidationsEnum> getValidationsEnums() {
		return validationsEnums;
	}
	public void setValidationsEnums(List<ValidationsEnum> validationsEnums) {
		this.validationsEnums = validationsEnums;
	}
	

}
