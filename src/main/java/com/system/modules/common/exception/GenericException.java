package com.system.modules.common.exception;

import java.util.List;

public class GenericException extends RuntimeException {

	private static final long serialVersionUID = 6919336495434333381L;

	private Enum<?> exception;
	private List<Enum<?>> exceptionsList;

	public GenericException(Enum<?> exception) {
		this.setException(exception);
	}

	public GenericException(List<Enum<?>> exceptionsList) {
		this.setExceptionsList(exceptionsList);
	}

	public Enum<?> getException() {
		return exception;
	}

	public void setException(Enum<?> exception) {
		this.exception = exception;
	}

	public List<Enum<?>> getExceptionsList() {
		return exceptionsList;
	}

	public void setExceptionsList(List<Enum<?>> exceptionsList) {
		this.exceptionsList = exceptionsList;
	}

}
