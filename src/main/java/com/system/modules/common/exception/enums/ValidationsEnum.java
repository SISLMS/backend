package com.system.modules.common.exception.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ValidationsEnum {
	Value_Duplicated("1000","ValueDuplicated"),
	USERNAME_DUPLICATED("1001","UserNameDuplicated"),
	
	
	USER_NOT_FOUND("2001","UserNotFound"),
	
	
	USERNAME_MISSING("3001","UserNameMissing"),

	
	USERNAME_INVALID("3001","UserNameInvalid");
	
	private String exceptionCode;
	private String exceptionKey;

	private ValidationsEnum(String exceptionCode,String exceptionKey) {
		this.exceptionCode=exceptionCode;
		this.exceptionKey=exceptionKey;
	}

	public String getExceptionCode() {
		return exceptionCode;
	}

	public String getExceptionKey() {
		return exceptionKey;
	}

	
}
