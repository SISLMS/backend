package com.system.modules.common.exception.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.system.modules.common.exception.DBValidationException;
import com.system.modules.common.exception.GenericException;
import com.system.modules.common.exception.enums.ValidationsEnum;

@ControllerAdvice
public class ExceptionControllerAdvice {
	
	@ExceptionHandler(DBValidationException.class)
	public ResponseEntity<List<ValidationsEnum>> dBValidationException(DBValidationException validationException){
		List<ValidationsEnum> validations = validationException.getValidationsEnums();
		return new ResponseEntity<List<ValidationsEnum>>(validations,HttpStatus.FORBIDDEN);
	}
	
	@ExceptionHandler(GenericException.class)
	public ResponseEntity<List<Enum<?>>> genericException(GenericException genericException){
		List<Enum<?>> validations = genericException.getExceptionsList();
		return new ResponseEntity<List<Enum<?>>>(validations,HttpStatus.FORBIDDEN);
	}
	
}
