package com.system.modules.core.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.system.modules.common.dto.JsTreeDTO;
import com.system.modules.common.enums.Days;
import com.system.modules.common.exception.DBValidationException;
import com.system.modules.common.exception.enums.ValidationsEnum;
import com.system.modules.common.model.BaseEntity;
import com.system.modules.common.service.BaseServiceImpl;
import com.system.modules.core.dao.CoreDao;
import com.system.modules.core.enums.ContainerClass;
import com.system.modules.core.model.ClassRoom;
import com.system.modules.core.model.Container;
import com.system.modules.core.model.ContainerGoals;
import com.system.modules.core.model.ContainerRole;
import com.system.modules.core.model.ContainerTableOfContents;
import com.system.modules.core.model.ContainerTableOfContentsWithContainer;
import com.system.modules.core.model.ContainerType;
import com.system.modules.core.model.Course;
import com.system.modules.core.model.Menu;
import com.system.modules.core.model.Role;
import com.system.modules.core.model.SystemInstance;
import com.system.modules.core.model.User;
import com.system.modules.core.model.UserContainer;
import com.system.modules.core.model.UserContainerRole;
import com.system.modules.schedualStructure.model.ClassRoomSection;
import com.system.modules.schedualStructure.model.SchedualStructure;
import com.system.modules.schedualStructure.model.SchedualStructurePeriod;
import com.system.modules.schedualStructure.model.Section;



@Service("coreService")
@Transactional
public class CoreServiceImpl extends BaseServiceImpl implements CoreService{

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private CoreDao coreDao;
	
	@Override
	public void saveContainerType(ContainerType containerType) {
		containerType.setCreationDate(new Date());
		coreDao.saveOrUpdate(containerType);
	}
	
	@Override
	public ContainerType getContainerTypeById(long contTypeId) {
		return coreDao.getObjectById(contTypeId, ContainerType.class);
	}
	
	public Boolean checkUniqueTrue(String tableName, String columnName, String value, Long id) {
		Long duplication = checkCountDuplicationOfValueInTable(tableName, columnName, value, id);
		if (duplication == 0l)
			return true;
		return false;
	}
	
	private Long checkCountDuplicationOfValueInTable(String tableName, String columnName, String value, Long id) {
		String sql = "select count(id) from " + tableName + " where " + columnName + " like '" + value + "'";
		if (id != null)
			sql += " and id <> " + id;
		Query query = getSession().createQuery(sql);
		return (Long) query.uniqueResult();
	}
	
	protected Session getSession(){
		return sessionFactory.getCurrentSession();
	}

	@Override
	public Days[] getDays() {
		Days[] days = Days.values();
		if (days.length > 0) {
			return days;
		}
		return null;
	}
	
	@Override
	public List<Container> getAllContainers() {
		return  coreDao.getAll(Container.class);
	}
	
	@Override
	public List<ClassRoom> getAllClassRooms() {
		return getAll(ClassRoom.class);
	}
	
	@Override
	public List<Course> getAllCourses(Long conId) {
		if(conId == null)
		return getAll(Course.class);
		return coreDao.getContainerCourses(conId);
	}
	
	@Override
	public Container getContainerById(long containerId) {
		return coreDao.getObjectById(containerId, Container.class);
	}
	
	@Override
	public ClassRoom getClassRoomById(long classRoomId) {
		return coreDao.getClassRoomById(classRoomId);
	}
	
	@Override
	public SystemInstance getSystemInstanceById(long systemInstanceId) {
		return coreDao.getObjectById(systemInstanceId, SystemInstance.class);
	}
	
	@Override
	public Course getCourseById(long courseId) {
		return coreDao.getObjectById(courseId, Course.class);
	}

	public void saveContainer(Container container) {
		container.setCreationDate(new Date());
    	container.setSuspended((container.getSuspended()==null)?false:container.getSuspended());
    	container.setStartDate(new Date());
    	coreDao.saveOrUpdate(container);
		System.out.println("<<<<<<<<<<<<<<<<<<< Container Saved >>>>>>>>>>>>>>>>>>>");
	}
	
	public void saveOrUpdateContainer(Container container) {
		container.setCreationDate(new Date());
    	container.setSuspended((container.getSuspended()==null)?false:container.getSuspended());
    	container.setStartDate(new Date());
    	coreDao.saveOrUpdate(container);
		System.out.println("<<<<<<<<<<<<<<<<<<< Container Saved >>>>>>>>>>>>>>>>>>>");
	}

	public void deleteContainer(long id) {
		coreDao.deleteUserContainerRolesByContainerId(id);
		coreDao.deleteUserContainerByContainerId(id);
		coreDao.deleteById(id, Container.class);
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<  Container "+id+" deleted >>>>>>>>>>>>>>>>>>>>>>>>>>");
	}
	
	@Override
	public List<ContainerGoals> getContainerGoals(long contId) {
		return coreDao.getContainerGoals(contId);
	}
	
	@Override
	public void saveSystemInstance(SystemInstance systemInstance) {
		systemInstance.setCreationDate(new Date());
		systemInstance.setSuspended((systemInstance.getSuspended()==null)?false:systemInstance.getSuspended());
		systemInstance.setStartDate(new Date());
    	coreDao.saveOrUpdate(systemInstance);
		System.out.println("<<<<<<<<<<<<<<<<<<< System Instance Saved >>>>>>>>>>>>>>>>>>>");
	}
	
	@Override
	public void createAssignedTableOfContents(List<ContainerTableOfContentsWithContainer> containerTableOfContentsWithContainer) {
		Set<Container> containers = new HashSet<Container>();
		for (ContainerTableOfContentsWithContainer tableOfContentsWithContainer : containerTableOfContentsWithContainer) {
			deleteContainerTableOfContents(tableOfContentsWithContainer.getAssignedContainer());
		}
		for (ContainerTableOfContentsWithContainer containerTableOfContents2 : containerTableOfContentsWithContainer) {
			saveBaseEntity(containerTableOfContents2);
		}
	}
	
	private void deleteContainerTableOfContents(Container container) {
		coreDao.deleteContainerTableOfContents(container);
	}

	@Override
	public void saveClassRoom(ClassRoom classRoom) {
		classRoom.setCreationDate(new Date());
		classRoom.setSuspended((classRoom.getSuspended()==null)?false:classRoom.getSuspended());
		classRoom.setStartDate(new Date());
    	coreDao.saveOrUpdate(classRoom);
		System.out.println("<<<<<<<<<<<<<<<<<<< System Class Room >>>>>>>>>>>>>>>>>>>");
	}
	
	@Override
	public void saveCourse(Course course) {
		course.setCreationDate(new Date());
		course.setSuspended((course.getSuspended()==null)?false:course.getSuspended());
		course.setStartDate(new Date());
    	coreDao.saveOrUpdate(course);
		System.out.println("<<<<<<<<<<<<<<<<<<< Course Saved >>>>>>>>>>>>>>>>>>>");
	}
	
	@Override
	public List<ContainerTableOfContentsWithContainer> getContainerAssignedTableOfContents(Section section) {
		return coreDao.getContainerAssignedTableOfContents(section);
	}
	
	@Override
	public void createCourseContent(long courseId, List<JsTreeDTO> courseContentJsTree) {
		Course course = getContainerById(courseId, Course.class);
		createContainerContent(course, courseContentJsTree);
	}
	
	@Override
	public void createContainerContent(Container container, List<JsTreeDTO> containerContentJsTree) {
		coreDao.deletePreviousContainerContents(container.getId());
		
		for (JsTreeDTO courseContent : containerContentJsTree) {
			ContainerTableOfContents tableOfContents = new ContainerTableOfContents();
			tableOfContents.setContainer(container);
			tableOfContents.setName(courseContent.getText());
			saveOrUpdate(tableOfContents);
			if (!courseContent.getChildren().isEmpty()) {
				createCourseContentRecursively(tableOfContents.getId(), container, courseContent.getChildren());
			}
		}
	}
	
	private void createCourseContentRecursively(Long parentId, Container container, List<JsTreeDTO> children) {
		for (int i = 0; i < children.size(); i++) {
			ContainerTableOfContents tableOfContentsChild = new ContainerTableOfContents();
			tableOfContentsChild.setParent(parentId);
			tableOfContentsChild.setContainer(container);
			tableOfContentsChild.setName(children.get(i).getText());
			saveOrUpdate(tableOfContentsChild);
			if (!children.get(i).getChildren().isEmpty()) {
				createCourseContentRecursively(tableOfContentsChild.getId(), container, children.get(i).getChildren());
			}
			
		}
	}
	
	@Override
	public <T extends BaseEntity> List<T>  getContainerChilds(Long containerId, ContainerClass containerClass) {
		T instance = (T) coreDao.getObjectById(containerId, Container.class);
		setContainerChildrensRecursive(instance, containerClass);
		List<T> childs = new ArrayList<T>();
		setChildrenContainerList(instance, childs);
		return childs;
	}
	
	private <T> void setChildrenContainerList(T container, List<T> childs) {
		for (Container child : ((Container) container).getChildContainers()) {
			childs.add((T) child);
			setChildrenContainerList((T) child, childs);
		}
	}

	private <T> void setContainerChildrensRecursive(T container, ContainerClass containerClass) {
		List<Container> childs = coreDao.getContainerChildsFirstLevel(((Container) container).getId(), containerClass);
		for (Container child : childs) {
			((Container) container).getChildContainers().add(child);
			setContainerChildrensRecursive(child, containerClass);
		}
	}
	
	

	public List<ContainerType> getAllContainerType() {
		return coreDao.getAll(ContainerType.class);
	}
	
	public List<Menu> fetchAllUserMenues(int roleId) {
		ArrayList<Menu> menuList = new ArrayList<Menu>();
		menuList.add(new Menu());
		return menuList;
	}
	
	public Role getRoleById(long roleId) {
		return coreDao.getObjectById(roleId, Role.class);
	}

	public List<Role> getAllRoles() {
		return coreDao.getAll(Role.class);
	}
	
	public void saveUserContainerRole(UserContainerRole userContainerRole) {
		coreDao.save(userContainerRole);
	}
	
	@Override
	public void assignRoleToUser(User user) {
		createUserContainerRoles(user, user.getContainerRoles());
		
	}
	
	@Override
	public void createUserContainerRoles(User user, List<ContainerRole> containerRoles) {
		deleteRolesAssignedToSpecificUser(user.getId());
		for (ContainerRole containerRole : containerRoles) {
			containerRole = coreDao.getContainerRolesByContainerIdAndRoleId(containerRole.getContainer().getId(), containerRole.getRole().getId());
			UserContainer userContainer = getUserContainer(containerRole.getContainer(), user);
			if (userContainer == null) {
				userContainer = new UserContainer();
				userContainer.setUser(user);
				userContainer.setContainer(containerRole.getContainer());
				coreDao.save(userContainer);
			}
			UserContainerRole userContainerRole = new UserContainerRole();
			userContainerRole.setUserContainer(userContainer);
			userContainerRole.setContainerRole(containerRole);
			coreDao.save(userContainerRole);
		}
	}
	
	public UserContainer getUserContainer(Container container, User usr) {
		return coreDao.getUserContainer(container.getId(), usr.getId());
	}

	@Override
	public void deleteRolesAssignedToSpecificUser(long userId) {
		coreDao.deleteUserContainerRolesByUserId(userId);
		coreDao.deleteUserContainerByUserId(userId);
	}
	
	@Override
	public List<UserContainerRole> getUserContainerRolesByUserID(Long userId) {
		return coreDao.getUserContainerRolesByUserID(userId);
	}
	
	public void saveUser(User user){
		user.setCreationDate(new Date());
        user.setSuspended((user.getSuspended()==null)?false:true);
        coreDao.saveOrUpdate(user);
		System.out.println("<<<<<<<<<<<<<<<<<<  User Saved  >>>>>>>>>>>>>>>>>");
	}
	
	public void deleteUserById(long id) {
		coreDao.deleteUserContainerRolesByUserId(id);
		coreDao.deleteUserContainerByUserId(id);
		coreDao.deleteById(id, User.class);
		System.out.println("<<<<<<<<<<<<<<<<<<  User Deleted  >>>>>>>>>>>>>>>>>");
	}

	@Override
	public List<Section> getPeriodSections(long classRoomId, long schedualStructurePeriodId) {
		return coreDao.getPeriodSections(classRoomId, schedualStructurePeriodId);
	}
	
	@Override
	public Section getCourseSection(long classRoomId, long schedualStructureId, long courseId) {
		Section section = coreDao.getCourseSection(classRoomId,schedualStructureId, courseId);
		if (section != null)
			section.setSectionUsers(getContainerUsers(section.getId()));
		else
			section = new Section();
		return section;
	}
	
	@Override
	public Section createSection(ClassRoom classRoom, SchedualStructurePeriod schedualStructurePeriod, Section section) {
		createClassRoomSection(classRoom, schedualStructurePeriod, section);
		String sectionName = (section.getId()!=null)?section.getId().toString():" ";
		sectionName += schedualStructurePeriod.getSchedualStructure().getName()+" "+section.getCourse().getName();
		section.setName(sectionName);
		saveOrUpdateContainer(section);
		if(section.getId() != null){
			coreDao.deleteUserContainerRolesByContainerId(section.getId());
			coreDao.deleteUserContainerByContainerId(section.getId());
		}
		for (User user : section.getSectionUsers()) {
			if(!user.getContainerRoles().isEmpty()){
			UserContainer userContainerSection = new UserContainer();
			userContainerSection.setUser(user);
			userContainerSection.setContainer(section);
			saveBaseEntity(userContainerSection);
			for (ContainerRole containerRoleSection : user.getContainerRoles()) {
				UserContainerRole userContainerRoleSection = new UserContainerRole();
				userContainerRoleSection.setUserContainer(userContainerSection);
				userContainerRoleSection.setContainerRole(containerRoleSection);
				saveBaseEntity(userContainerRoleSection);
			}
			if (section.getCourse() != null) {
				if(section.getId() != null){
					coreDao.deleteUserContainerRolesByContainerIdAndUserId(section.getCourse().getId(), user.getId());
					coreDao.deleteUserContainerByContainerIdAndUserId(section.getCourse().getId(), user.getId());
				}
				UserContainer userContainerCourse = new UserContainer();
				userContainerCourse.setUser(user);
				userContainerCourse.setContainer(getContainerById(section.getCourse().getId()));
				saveBaseEntity(userContainerCourse);
				for (ContainerRole containerRoleCourse : user.getContainerRoles()) {
					UserContainerRole userContainerRoleCourse = new UserContainerRole();
					userContainerRoleCourse.setUserContainer(userContainerCourse);
					userContainerRoleCourse.setContainerRole(containerRoleCourse);
					saveBaseEntity(userContainerRoleCourse);
				}
			}
			}
		}
		return section;
	}
	
	@Override
	public void createClassRoomSection(ClassRoom classRoom, SchedualStructurePeriod schedualStructurePeriod, Section section) {
		ClassRoomSection classRoomSection = new ClassRoomSection();
		classRoomSection.setPeriod(schedualStructurePeriod);
		classRoomSection.setSection(section);
		classRoomSection.setClassRoom(classRoom);
		saveBaseEntity(classRoomSection);
	}
	
	@Override
	public ClassRoomSection getClassRoomSection(ClassRoom classRoom, SchedualStructurePeriod schedualStructurePeriod, Section section) {
		return coreDao.getClassRoomSection(classRoom, schedualStructurePeriod, section);
	}
	
	public List<UserContainer> getUserRoles(long id) {
		return  coreDao.getUserContainersByUserId(id);
		 
	}
	
	@Override
	public <T extends BaseEntity> T getContainerById(Long containerId, Class<T> calzz) {
		return coreDao.getContainerById(containerId, calzz);
	}
	
	public List<User> getAllUsers() {
		return  coreDao.getAll(User.class);
	}
	
	@Override
	public List<User> getContainerUsers(long containerId) {
		return coreDao.getContainerUsers(containerId);
	}
	
	public User getUserById(long id){
		return coreDao.getObjectById(id, User.class);
	}

	public Boolean checkUniqueTrue(String columnName, String value, Long id) {
		if (id != null) {
			User user = coreDao.getObjectById(id, User.class);
			if (user.getUserName().equalsIgnoreCase(value)) {
				return true;
			} else {
				Long duplication = coreDao.checkCountDuplicationOfValueInUserTable(columnName, value);
				if (duplication == 0l) {
					return true;
				}
			}
		} else {
			Long duplication = coreDao.checkCountDuplicationOfValueInUserTable(columnName, value);
			if (duplication == 0l) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void validateSaveUserToDB(User userValidate) {
		List<ValidationsEnum> validations = new ArrayList<ValidationsEnum>();
		Boolean uniqueUserName = checkUniqueTrue("User", "userName", userValidate.getName(), userValidate.getId());
 		if (!uniqueUserName)
 			validations.add(ValidationsEnum.USERNAME_DUPLICATED);
 		if (validations.size()>0)
 		throw new DBValidationException(validations);
	}

	
	public CoreDao getCoreDao() {
		return coreDao;
	}

	public void setCoreDao(CoreDao coreDao) {
		this.coreDao = coreDao;
	}
	
	@Override
	public void removeAllRelatedUsersToContainer(ClassRoom classRoom) {
		coreDao.deleteUserContainerRolesByContainerId(classRoom.getId());
		coreDao.deleteUserContainerByContainerId(classRoom.getId());
	}
	
	@Override
	public Container childrenContainersHierarchyForContainer(Long containerId) {
		List<Container> containerList=coreDao.getAll(Container.class);
		Container rootContainer = null;
			for(int i=0;i<containerList.size();i++){
				if(containerList.get(i).getId().equals(containerId))
					rootContainer=containerList.get(i);
				if(containerList.get(i).getParentContainer()!=null){
					Long parentContainerId = containerList.get(i).getParentContainer().getId();
					for(int j=0;j<containerList.size();j++){
						if (containerList.get(j).getId().equals(parentContainerId)){
							if(containerList.get(j).getChildContainers()==null)
								containerList.get(j).setChildContainers(new ArrayList<Container>());
							if(!containerList.get(j).getChildContainers().contains(containerList.get(i)))
							containerList.get(j).getChildContainers().add(containerList.get(i));
							
						}
					}					
				}

			}	
		
		
		return rootContainer;
	}
	
	/*@Override
	public List<Section> getPeriodSections(long classRoomId, long periodId) {
		List<Section> sections = coreDao.getPeriodSections(classRoomId, periodId);
		for (Section section : sections) {
			section.setSectionUsers(getContainerUsers(section.getId()));
		}
		return sections;
	}*/
	
	/*@Override
	public List<Children> getContainerWithChildrenJsTreeDTO(Long containerId) {
		Container container = childrenContainersHierarchyForContainer(containerId);
		List<JsTreeDTO.Children> childrens = new ArrayList<>();
		Children children = new Children();
		children.setId(String.valueOf(containerId));
		children.setParent("#");
		children.setText(container.getName());
		childrens.add(children);
		buildJsTreeDTORecursivly(childrens, container);
		return childrens;
		
	}*/
	
	/*public void buildJsTreeDTORecursivly(List<Children> childrens, Container container) {
        // do something with the current node instead of System.out
        List<Container> containers = container.getSubContainerList();
        
        if (containers != null) {
        	for (int i = 0; i < containers.size(); i++) {
                Container currentNode = containers.get(i);
                
                JsTreeDTO.Children children = new JsTreeDTO.Children();
				children.setId(String.valueOf(currentNode.getId()));
				children.setText(currentNode.getName());
				children.setParent(currentNode.getParentContainer().getId().toString());
				childrens.add(children);
                //calls this method for all the children which is Element
                System.out.println(currentNode.getName());
                buildJsTreeDTORecursivly(childrens, currentNode);
				
            }
		}
    }*/
	
	@Override
	public List<ContainerTableOfContents> getcontainerContents(long containerId) {
		return coreDao.getcontainerContents(containerId);
	}
	
	@Override
	public List<ContainerRole> getContainerRolesByContainerId(long containerId) {
		return coreDao.getContainerRolesByContainerId(containerId);
	}
	@Override
	public List<ContainerRole> getContainerRolesForUser(long userId) {
		return coreDao.getContainerRolesForUser(userId);
	}
	@Override
	public List<ContainerType> getContainerTypeRelatedToSpecificContainer(Long containerId) {
		return coreDao.getContainerTypeRelatedToSpecificContainer(containerId);
	}	
	
	@Override
	public List<ContainerType> getAllSystemInstanceTypes() {
		return coreDao.getAllSystemInstanceTypes();
	}
	
	@Override
	public List<ContainerType> containerTypesHierarchyForUser(Long loggedInUserId) {
		return coreDao.getContainerTypesHierarchyForUser(loggedInUserId);
	}
}
