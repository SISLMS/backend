package com.system.modules.core.service;

import java.util.List;
import com.system.modules.common.dto.JsTreeDTO;
import com.system.modules.common.enums.Days;
import com.system.modules.common.model.BaseEntity;
import com.system.modules.common.service.BaseService;
import com.system.modules.core.enums.ContainerClass;
import com.system.modules.core.model.ClassRoom;
import com.system.modules.core.model.Container;
import com.system.modules.core.model.ContainerGoals;
import com.system.modules.core.model.ContainerRole;
import com.system.modules.core.model.ContainerTableOfContents;
import com.system.modules.core.model.ContainerTableOfContentsWithContainer;
import com.system.modules.core.model.ContainerType;
import com.system.modules.core.model.Course;
import com.system.modules.core.model.Menu;
import com.system.modules.core.model.Role;
import com.system.modules.core.model.SystemInstance;
import com.system.modules.core.model.User;
import com.system.modules.core.model.UserContainer;
import com.system.modules.core.model.UserContainerRole;
import com.system.modules.schedualStructure.model.ClassRoomSection;
import com.system.modules.schedualStructure.model.SchedualStructure;
import com.system.modules.schedualStructure.model.SchedualStructurePeriod;
import com.system.modules.schedualStructure.model.Section;

public interface CoreService extends BaseService{
	
	Boolean checkUniqueTrue(String tableName, String columnName, String value, Long id);

	void saveContainerType(ContainerType containerType);

	ContainerType getContainerTypeById(long contTypeId);

	Days[] getDays();

	List<Container> getAllContainers();

	Container getContainerById(long containerId);

	void saveContainer(Container container);

	void deleteContainer(long id);

	List<ContainerType> getAllContainerType();

	public List<Menu> fetchAllUserMenues(int roleId);

	Role getRoleById(long roleId);

	List<Role> getAllRoles();

	void saveUserContainerRole(UserContainerRole userContainerRole);

	UserContainer getUserContainer(Container container, User usr);

	void deleteRolesAssignedToSpecificUser(long userId);
	
	List<User> getAllUsers();

	void saveUser(User user);
	
	void deleteUserById(long id);
	
	List<UserContainer> getUserRoles(long id);
	
	User getUserById(long userId);

	Boolean checkUniqueTrue(String columnName, String value, Long id);

	List<UserContainerRole> getUserContainerRolesByUserID(Long id);
	
	void validateSaveUserToDB(User userValidate);

	List<ContainerType> getContainerTypeRelatedToSpecificContainer(Long containerId);

	List<ContainerType> containerTypesHierarchyForUser(Long loggedInUserId);

	Container childrenContainersHierarchyForContainer(Long containerId);

//	List<Children> getContainerWithChildrenJsTreeDTO(Long containerId);

	void saveSystemInstance(SystemInstance systemInstance);

	SystemInstance getSystemInstanceById(long systemInstanceId);

	ClassRoom getClassRoomById(long classRoomId);

	void saveClassRoom(ClassRoom classRoom);

	List<ClassRoom> getAllClassRooms();

	void saveCourse(Course course);

	Course getCourseById(long courseId);

	List<Course> getAllCourses(Long conId);

	List<User> getContainerUsers(long containerId);

	List<ContainerRole> getContainerRolesByContainerId(long containerId);

	void removeAllRelatedUsersToContainer(ClassRoom classRoom);

	Section getCourseSection(long classRoomId, long schedualStructureId, long courseId);

	List<ContainerGoals> getContainerGoals(long contId);

	void createContainerContent(Container container, List<JsTreeDTO> containerContentJsTree);
	
	<T extends BaseEntity> T getContainerById(Long containerId, Class<T> calzz);

	void createCourseContent(long courseId, List<JsTreeDTO> courseContentJsTree);

	List<ContainerTableOfContents> getcontainerContents(long containerId);

	Section createSection(ClassRoom classRoom, SchedualStructurePeriod schedualStructurePeriod, Section section);

	void createClassRoomSection(ClassRoom classRoom, SchedualStructurePeriod schedualStructurePeriod, Section section);

	List<Section> getPeriodSections(long classRoomId, long schedualStructurePeriodId);

	void createAssignedTableOfContents(List<ContainerTableOfContentsWithContainer> containerTableOfContentsWithContainer);

	List<ContainerTableOfContentsWithContainer> getContainerAssignedTableOfContents(Section section);

	<T extends BaseEntity> List<T> getContainerChilds(Long containerId, ContainerClass containerClass);

	List<ContainerType> getAllSystemInstanceTypes();

	void createUserContainerRoles(User user, List<ContainerRole> containerRoles);

	void assignRoleToUser(User user);

	List<ContainerRole> getContainerRolesForUser(long userId);

	ClassRoomSection getClassRoomSection(ClassRoom classRoom, SchedualStructurePeriod schedualStructurePeriod,
			Section section);

}
