package com.system.modules.core.controller;
 
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.system.modules.common.dto.JsTreeDTO;
import com.system.modules.common.enums.Days;
import com.system.modules.common.exception.GenericException;
import com.system.modules.common.exception.enums.ValidationsEnum;
import com.system.modules.core.enums.ContainerClass;
import com.system.modules.core.model.ClassRoom;
import com.system.modules.core.model.Container;
import com.system.modules.core.model.ContainerGoals;
import com.system.modules.core.model.ContainerRole;
import com.system.modules.core.model.ContainerTableOfContents;
import com.system.modules.core.model.ContainerTableOfContentsWithContainer;
import com.system.modules.core.model.ContainerType;
import com.system.modules.core.model.Course;
import com.system.modules.core.model.Menu;
import com.system.modules.core.model.Role;
import com.system.modules.core.model.SystemInstance;
import com.system.modules.core.model.User;
import com.system.modules.core.model.UserContainer;
import com.system.modules.core.model.UserContainerRole;
import  com.system.modules.core.service.CoreService;
import com.system.modules.schedualStructure.model.ClassRoomSection;
import com.system.modules.schedualStructure.model.SchedualStructurePeriod;
import com.system.modules.schedualStructure.model.SchedualStructurePeriodInstance;
import com.system.modules.schedualStructure.model.Section;
import com.system.modules.schedualStructure.service.SchedualStructureService;


@RestController
@RequestMapping("/api/coreController")
@Transactional
public class CoreController {
 
	@Autowired
	CoreService coreService;
	
	@Autowired
	SchedualStructureService schedualStructureService;
    
    
	@RequestMapping(value = "/allMess/", method = RequestMethod.GET)
    public ResponseEntity<List<ValidationsEnum>> listAllMessageError() {
    	List<ValidationsEnum> validations = new ArrayList<ValidationsEnum>();
    	validations.add(ValidationsEnum.USER_NOT_FOUND);
    	validations.add(ValidationsEnum.USERNAME_DUPLICATED);
    	return new ResponseEntity<List<ValidationsEnum>>(validations, HttpStatus.OK);
    }
	
	
    //*********************************** User Management ***********************************
    
    //----------------------------   Retrieve All Users  -------------------------------------------
    
    @RequestMapping(value = "/allUsers/", method = RequestMethod.GET)
    public ResponseEntity<List<User>> listAllUsers() {
        List<User> users = coreService.getAllUsers();
        if(users.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }
    
    //-------------------   Retrieve All Users Assigned To Container ----------------------------------
    
    @RequestMapping(value = "/getUsersAssignedToContainer/{containerId}", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getUsersAssignedToContainer(@PathVariable("containerId") long containerId) {
        List<User> users = coreService.getContainerUsers(containerId);
        if(users.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getCourseSection/{classRoomId}/{schedualStructureId}/{courseId}", method = RequestMethod.GET)
    public ResponseEntity<Section> getCourseSection(@PathVariable("classRoomId") long classRoomId, @PathVariable("schedualStructureId") long schedualStructureId, @PathVariable("courseId") long courseId) {
    	Section section = coreService.getCourseSection(classRoomId, schedualStructureId, courseId);
    	return new ResponseEntity<Section>(section, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getPeriodSections/{classRoomId}/{schedualStructurePeriodId}", method = RequestMethod.GET)
    public ResponseEntity<List<Section>> getPeriodSections(@PathVariable("classRoomId") long classRoomId, @PathVariable("schedualStructurePeriodId") long schedualStructurePeriodId) {
    	List<Section> sections = coreService.getPeriodSections(classRoomId, schedualStructurePeriodId);
    	return new ResponseEntity<List<Section>>(sections, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/createSection/{schedualStructurePeriodId}/{classRoomId}", method = RequestMethod.POST)
    public Section createSection(@PathVariable("schedualStructurePeriodId") long schedualStructurePeriodId, @PathVariable("classRoomId") long classRoomId, @RequestBody Section section) {
    	SchedualStructurePeriod schedualStructurePeriod = schedualStructureService.getObjectById(schedualStructurePeriodId, SchedualStructurePeriod.class);
    	ClassRoom classRoom = schedualStructureService.getObjectById(classRoomId, ClassRoom.class);
    	section = coreService.createSection(classRoom, schedualStructurePeriod, section);
    	return section;
    }
    
    //------------------- Get a User By id --------------------------------------------
    
    @RequestMapping(value = "/getUserById/{userId}", method = RequestMethod.GET)
    public ResponseEntity<User> getUserById(@PathVariable("userId") long userId) {
        User user = coreService.getUserById(userId);
        return new ResponseEntity<User>(user,HttpStatus.OK);
    }
    
    //--------------------------------- Create a New User ------------------------------------------
    
    @RequestMapping(value = "/createUser/", method = RequestMethod.POST)
    public ResponseEntity<Void> createNewUser(@RequestBody User user) {
    	HttpHeaders headers = new HttpHeaders();
//    	List<Integer> assignedroles = user.getContainerRoleIds();
    	try {
    		coreService.validateSaveUserToDB(user);
    		coreService.saveUser(user);
    		coreService.assignRoleToUser(user);
		} 
    	catch (Exception e) {
    		e.printStackTrace();
    	}
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
    //------------------- Delete a User --------------------------------------------
    
    @RequestMapping(value = "/deleteUser/{id}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable("id") long id) {
        System.out.println("Fetching & Deleting User with id " + id);
        coreService.deleteUserById(id);
    }
    
    //------------------------------  Get All Roles   ------------------------------
    @RequestMapping(value="/getRoles/" , method=RequestMethod.GET)
    public ResponseEntity<List<Role>>  getRoles(){
    	List<Role> roles = coreService.getAllRoles();
    	System.out.println("Return All Roles");
    	return new ResponseEntity<List<Role>>(roles,HttpStatus.OK);
    }
    
    //-------------------   Retrieve All Roles Assigned To Container ----------------------------------
    
    @RequestMapping(value = "/getContainerRoles/{containerId}", method = RequestMethod.GET)
    public ResponseEntity<List<ContainerRole>> getContainerRoles(@PathVariable("containerId") long containerId) {
        List<ContainerRole> roles = coreService.getContainerRolesByContainerId(containerId);
        if(roles.isEmpty()){
            return new ResponseEntity<List<ContainerRole>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<ContainerRole>>(roles, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getContainerRolesForUser/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<ContainerRole>> getContainerRolesForUser(@PathVariable("userId") long userId) {
        List<ContainerRole> roles = coreService.getContainerRolesForUser(userId);
        if(roles.isEmpty()){
            return new ResponseEntity<List<ContainerRole>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<ContainerRole>>(roles, HttpStatus.OK);
    }
    
    //------------------  Get All Roles Assigned To Specific User  -------------------    
    @RequestMapping(value="/getRolesAssignedToSpecificUser/{id}" , method=RequestMethod.GET)
    public ResponseEntity<List<UserContainer>> getRolesAssignedToSpecificUser(@PathVariable("id") long id){
    	List<UserContainer> userContainersList = coreService.getUserRoles(id);
    	return new ResponseEntity<List<UserContainer>>(userContainersList,HttpStatus.OK);
    }
    
    
    
 //*********************************** Container Management ***********************************
 
    //--------------------------------  Retrieve All Containers  ----------------------------------
    
    @RequestMapping(value = "/allcontainers/", method = RequestMethod.GET)
    public ResponseEntity<List<Container>> listAllContainers() {
        List<Container> containers = coreService.getAllContainers();
        if(containers.isEmpty()){
            return new ResponseEntity<List<Container>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Container>>(containers, HttpStatus.OK);
    }
    
    //--------------------------------  Retrieve All System Instances  ----------------------------------
    
    @RequestMapping(value = "/allSystemInstances/", method = RequestMethod.GET)
    public ResponseEntity<List<SystemInstance>> listAllSystemInstances() {
        List<SystemInstance> systemInstances = coreService.getAll(SystemInstance.class);
        if(systemInstances.isEmpty()){
            return new ResponseEntity<List<SystemInstance>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<SystemInstance>>(systemInstances, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/allChildsSystemInstances/{startFromId}", method = RequestMethod.GET)
    public ResponseEntity<List<SystemInstance>> listAllChildsSystemInstances(@PathVariable("startFromId") Long startFromId) {
        List<SystemInstance> systemInstances = coreService.getContainerChilds(startFromId, ContainerClass.SYSTEMINSTANCE);
        if(systemInstances.isEmpty()){
            return new ResponseEntity<List<SystemInstance>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<SystemInstance>>(systemInstances, HttpStatus.OK);
    }
    
    //------------------- Get a System Instance By id --------------------------------------------
    
    @RequestMapping(value = "/getSystemInstanceById/{sysInsId}", method = RequestMethod.GET)
    public ResponseEntity<SystemInstance> getSystemInstanceById(@PathVariable("sysInsId") long systemInstanceId) {
    	SystemInstance systemInstance = coreService.getSystemInstanceById(systemInstanceId);
        return new ResponseEntity<SystemInstance>(systemInstance, HttpStatus.OK);
    }
    
    //----------------------------------  Create a New System Instance  ----------------------------------
    
    @RequestMapping(value = "/newSystemInstance/", method = RequestMethod.POST)
	public ResponseEntity<Void> createSystemInstance(@RequestBody SystemInstance systemInstance) {
		HttpHeaders headers = new HttpHeaders();
		try {
			coreService.saveSystemInstance(systemInstance);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
    
    //--------------------------------  Retrieve All Class Room  ----------------------------------
    
    @RequestMapping(value = "/allClassRooms/", method = RequestMethod.GET)
    public ResponseEntity<List<ClassRoom>> listAllClassRooms() {
        List<ClassRoom> classRooms = coreService.getAllClassRooms();
        if(classRooms.isEmpty()){
            return new ResponseEntity<List<ClassRoom>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<ClassRoom>>(classRooms, HttpStatus.OK);
    }
    
    //--------------------------------  Retrieve All Course  ----------------------------------
    
    @RequestMapping(value = "/allCourses/{conId}", method = RequestMethod.GET)
    public ResponseEntity<List<Course>> listAllCourses(@PathVariable("conId") Long conId) {
        List<Course> courses = coreService.getAllCourses(conId);
        if(courses.isEmpty()){
            return new ResponseEntity<List<Course>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Course>>(courses, HttpStatus.OK);
    }
    
    
    //------------------- Containers With Thier Roles ------------------------------
    
    @RequestMapping(value = "/getContainersHierarchyWithThierRoles/{id}", method = RequestMethod.GET)
    public List<JsTreeDTO>  getContainersHierarchyWithThierRoles(@PathVariable("id") long currentContainerId) {
    	List<JsTreeDTO> containersWithThierRoles = new ArrayList<JsTreeDTO>();
    	List<SystemInstance> systemInstances = coreService.getContainerChilds(currentContainerId, ContainerClass.SYSTEMINSTANCE);
    	for (SystemInstance systemInstance : systemInstances) {
    		JsTreeDTO jsTreeDTO = new JsTreeDTO();
    		jsTreeDTO.setEntityId(systemInstance.getId().toString());
    		jsTreeDTO.setEntityTypeName("container");
    		jsTreeDTO.setText(systemInstance.getName());
    		List<ContainerRole> containerRoles = coreService.getContainerRolesByContainerId(systemInstance.getId());
    		for (ContainerRole containerRole : containerRoles) {
    			JsTreeDTO jsTreeDTO2 = new JsTreeDTO();
        		jsTreeDTO2.setEntityId(containerRole.getRole().getId().toString());
        		jsTreeDTO2.setEntityParentId(containerRole.getContainer().getId().toString());
        		jsTreeDTO2.setEntityTypeName("role");
        		jsTreeDTO2.setText(containerRole.getRole().getName());
        		jsTreeDTO.getChildren().add(jsTreeDTO2);
			}
    		containersWithThierRoles.add(jsTreeDTO);
		}
    	return containersWithThierRoles;
    }
 
    
    //------------------- Get a Container By id --------------------------------------------
    
    @RequestMapping(value = "/getContainerById/{contId}", method = RequestMethod.GET)
    public ResponseEntity<Container> getContainerById(@PathVariable("contId") long contId) {
        Container container = coreService.getContainerById(contId);
        return new ResponseEntity<Container>(container,HttpStatus.OK);
    }
    
    //------------------- Get a Container Goals  --------------------------------------------
    
    @RequestMapping(value = "/getContainerGoals/{contId}", method = RequestMethod.GET)
    public ResponseEntity<List<ContainerGoals>> getContainerGoals(@PathVariable("contId") long contId) {
    	List<ContainerGoals> containerGoals = coreService.getContainerGoals(contId);
        return new ResponseEntity<List<ContainerGoals>>(containerGoals,HttpStatus.OK);
    }
    
    //----------------------------------  Create a New Container  ----------------------------------
    
    @RequestMapping(value = "/newContainerGoal/", method = RequestMethod.POST)
	public ResponseEntity<Void> newContainerGoal(@RequestBody ContainerGoals containerGoal) {
		HttpHeaders headers = new HttpHeaders();
		try {
			containerGoal.setContainer(coreService.getContainerById(containerGoal.getContainer().getId()));
			coreService.saveBaseEntity(containerGoal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
    
    //------------------- Get a Class Room By id --------------------------------------------
    
    @RequestMapping(value = "/getClassRoomById/{classRoomId}", method = RequestMethod.GET)
    public ResponseEntity<ClassRoom> getClassRoomById(@PathVariable("classRoomId") long classRoomId) {
    	ClassRoom classRoom = coreService.getClassRoomById(classRoomId);
        return new ResponseEntity<ClassRoom>(classRoom, HttpStatus.OK);
    }
    
    //------------------- Get a Course By id --------------------------------------------
    
    @RequestMapping(value = "/getCourseById/{courseId}", method = RequestMethod.GET)
    public ResponseEntity<Course> getCourseById(@PathVariable("courseId") long courseId) {
    	Course course = coreService.getCourseById(courseId);
        return new ResponseEntity<Course>(course, HttpStatus.OK);
    }
    
    //----------------------------------  Create a New Container  ----------------------------------
    
    @RequestMapping(value = "/newcontainer/", method = RequestMethod.POST)
	public ResponseEntity<Void> createNewContainer(@RequestBody Container container) {
		HttpHeaders headers = new HttpHeaders();
		try {
			coreService.saveContainer(container);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
    
    //----------------------------------  Create a New Course  ----------------------------------
    
    @RequestMapping(value = "/newCourse/", method = RequestMethod.POST)
	public ResponseEntity<Void> createNewCourse(@RequestBody Course course) {
		HttpHeaders headers = new HttpHeaders();
		try {
			coreService.saveCourse(course);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
    
    //-------------------------------- Create a New Course Content  ------------------------------
    
    @RequestMapping(value = "/newCourseContent/{courseId}", method = RequestMethod.POST)
	public ResponseEntity<Void> createNewCourseContent(@PathVariable("courseId") long courseId, @RequestBody List<JsTreeDTO> courseContentJsTree) {
		HttpHeaders headers = new HttpHeaders();
		try {
			coreService.createCourseContent(courseId, courseContentJsTree);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
    
    @RequestMapping(value = "/listContainerContents/{containerId}", method = RequestMethod.GET)
    public ResponseEntity<List<ContainerTableOfContents>> listCourseContents(@PathVariable("containerId") long containerId) {
    	List<ContainerTableOfContents> containerTableOfContents = coreService.getcontainerContents(containerId);
        return new ResponseEntity<List<ContainerTableOfContents>>(containerTableOfContents, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/listCoursesContentBySection/", method = RequestMethod.POST)
    public ResponseEntity<List<JsTreeDTO>> listCourseContents(@RequestBody List<Section> sections) {
    	List<JsTreeDTO> jsTreeDTOs = new ArrayList<JsTreeDTO>();
    	for (Section section : sections) {
    		List<ContainerTableOfContentsWithContainer> selectedNodes = new ArrayList<ContainerTableOfContentsWithContainer>();
    		selectedNodes = coreService.getContainerAssignedTableOfContents(section);
    		List<ContainerTableOfContents> containerTableOfContents = new ArrayList<ContainerTableOfContents>();
    		containerTableOfContents.addAll(coreService.getcontainerContents(section.getCourse().getId()));
    		for (ContainerTableOfContents containerTableOfContents2 : containerTableOfContents) {
				JsTreeDTO jsTreeDTO = new JsTreeDTO();
//				jsTreeDTO.setId(containerTableOfContents2.getId().toString());
//				jsTreeDTO.setParent(containerTableOfContents2.getParent()!=null?containerTableOfContents2.getParent().toString():"#");
				jsTreeDTO.setText(containerTableOfContents2.getName());
//				jsTreeDTO.setContainer(containerTableOfContents2.getContainer());
				for (ContainerTableOfContentsWithContainer containerTableOfContentsWithContainer : selectedNodes) {
					if(containerTableOfContents2.getId() == containerTableOfContentsWithContainer.getContainerTableOfContent().getId()){
						JsTreeDTO.State state = new JsTreeDTO.State();
						state.setSelected(true);
						jsTreeDTO.setState(state);
						}
				}
				jsTreeDTOs.add(jsTreeDTO);
			}
		}
        return new ResponseEntity<List<JsTreeDTO>>(jsTreeDTOs, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/createAssignedTableOfContents/", method = RequestMethod.POST)
    public void createAssignedTableOfContents(@RequestBody List<ContainerTableOfContentsWithContainer> containerTableOfContentsWithContainer) {
    	coreService.createAssignedTableOfContents(containerTableOfContentsWithContainer);
    }
    
    //----------------------------------  Create a Class Room  ----------------------------------
    
    @RequestMapping(value = "/newClassRoom/", method = RequestMethod.POST)
	public ResponseEntity<Void> createNewClassRoom(@RequestBody ClassRoom classRoom) {
    	if(classRoom.getId() != null){
    		coreService.removeAllRelatedUsersToContainer(classRoom);
    	}
    	coreService.saveOrUpdate(classRoom);
    	for (User user : classRoom.getUsers()) {
    		UserContainer userContainer = new UserContainer();
    		userContainer.setUser(user);
    		userContainer.setContainer(classRoom);
    		coreService.saveBaseEntity(userContainer);
    		for (ContainerRole containerRole : user.getContainerRoles()) {
				UserContainerRole userContainerRole = new UserContainerRole();
				userContainerRole.setUserContainer(userContainer);
				userContainerRole.setContainerRole(containerRole);
				coreService.saveBaseEntity(userContainerRole);
			}
		}
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
    
    
    @RequestMapping(value = "/getContainerTypesRelatedToSpecificContainer/{containerId}", method = RequestMethod.GET)
    public ResponseEntity<List<ContainerType>> getContainerTypesRelatedToSpecificContainer(@PathVariable("containerId") Long containerId) {
        List<ContainerType> parentContainer = coreService.getContainerTypeRelatedToSpecificContainer(containerId);
        if(parentContainer.size()<1){
            return new ResponseEntity<List<ContainerType>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<ContainerType>>(parentContainer, HttpStatus.OK);
    }
    
    //--------------------------------------   Delete Container   --------------------------------------
    
    @RequestMapping(value = "/deleteContainer/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Container> deleteContainer(@PathVariable("id") long id) {
        System.out.println("Deleting Container with id " + id);
        coreService.deleteContainer(id);
        return new ResponseEntity<Container>(HttpStatus.NO_CONTENT);
    }
    
    
    //*********************************   Container Type Managment   ********************************
    
    
    //----------------------------------  Create a New Container Type  ----------------------------------
    
    @RequestMapping(value = "/newContainerType/", method = RequestMethod.POST)
	public void createNewContainerType(@RequestBody ContainerType containerType) {
		coreService.saveContainerType(containerType);
	}
    
    
    //----------------------------------  Retrieve All Container Types  ----------------------------------

    @RequestMapping(value = "/availableContainerTypes/", method = RequestMethod.GET)
    public ResponseEntity<List<ContainerType>> listAllAvailableContainerTypes() {
        List<ContainerType> containerTypes = coreService.getAllContainerType();
        if(containerTypes.size()<1){
            return new ResponseEntity<List<ContainerType>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<ContainerType>>(containerTypes, HttpStatus.OK);
    }
    
    //----------------------------------  Retrieve All System Instance Types  ----------------------------------

    @RequestMapping(value = "/availableSystemInstanceTypes/", method = RequestMethod.GET)
    public List<ContainerType> listAllAvailableSystemInstanceTypes() {
        List<ContainerType> containerTypes = coreService.getAllSystemInstanceTypes();
        return containerTypes;
    }
    
    @RequestMapping(value = "/containersTypeHierarchyForUser/{loggedInUserId}", method = RequestMethod.GET)
    public ResponseEntity<List<ContainerType>> containerTypesHierarchyForUser(@PathVariable("loggedInUserId") Long loggedInUserId) {
        List<ContainerType> parentTypes = coreService.containerTypesHierarchyForUser(loggedInUserId);
        if(parentTypes.size()<1){
            return new ResponseEntity<List<ContainerType>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<ContainerType>>(parentTypes, HttpStatus.OK);
    }
    
    //------------------- Get a Container Type By id --------------------------------------------
    
    @RequestMapping(value = "/getContainerTypeById/{contTypeId}", method = RequestMethod.GET)
    public ResponseEntity<ContainerType> getContainerTypeById(@PathVariable("contTypeId") long contTypeId) {
    	ContainerType containerType = coreService.getContainerTypeById(contTypeId);
        return new ResponseEntity<ContainerType>(containerType,HttpStatus.OK);
    }
    
    //********************************** Schedual Structure Managments *****************************
    
    @RequestMapping(value = "/Days", method = RequestMethod.GET)
    public ResponseEntity<Days[]> availableDays() {
    	Days[] days = coreService.getDays();
    	if (days.length > 0) {
    		return new ResponseEntity<Days[]>(days, HttpStatus.OK);
		}
    	return null;
    }
    
    
    //******************************************Global Services**************************************
    
    // ---------------------------------------- Check Uniqness --------------------------------------
    
    @RequestMapping(value="/checkUniqueValue/",method=RequestMethod.POST)
    @Produces(MediaType.APPLICATION_JSON)
    public String checkUniqueValue(@RequestBody String[] data){
    	String tableName = data[0];
    	String columnName = data[1];
    	Long id ;
    	if (data[2] != null)
    		 id = Long.parseLong(data[2]);
    	else
    		id = null;
    	
    	String value = data[3];
    	Boolean uniqness = null;
		uniqness = coreService.checkUniqueTrue(tableName, columnName, value, id);
		if (uniqness)
			return "true";
		
		List<Enum<?>> resultedExceptions = new ArrayList<Enum<?>>();
		resultedExceptions.add(ValidationsEnum.Value_Duplicated);
		throw new GenericException(resultedExceptions);
    }
    
    //------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/personality/menues", method = RequestMethod.GET)
    public ResponseEntity<List<Menu>> listAllUserMenues() {
        List<Menu> menues = coreService.fetchAllUserMenues(5);
        if(menues.isEmpty()){
            return new ResponseEntity<List<Menu>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Menu>>(menues, HttpStatus.OK);
    }
    
}