package com.system.modules.core.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ContainerClass {
	ROOTCONTAINER,
	COURSE,
	CLASSROOM,
	SYSTEMINSTANCE,
	SECTION,
	CONTAINER;
}
