package com.system.modules.core.dao;

import java.util.List;
import com.system.modules.common.dao.BaseDao;
import com.system.modules.core.enums.ContainerClass;
import com.system.modules.core.model.Category;
import com.system.modules.core.model.ClassRoom;
import com.system.modules.core.model.Container;
import com.system.modules.core.model.ContainerGoals;
import com.system.modules.core.model.ContainerRole;
import com.system.modules.core.model.ContainerTableOfContents;
import com.system.modules.core.model.ContainerTableOfContentsWithContainer;
import com.system.modules.core.model.ContainerType;
import com.system.modules.core.model.Course;
import com.system.modules.core.model.User;
import com.system.modules.core.model.UserContainer;
import com.system.modules.core.model.UserContainerRole;
import com.system.modules.schedualStructure.model.ClassRoomSection;
import com.system.modules.schedualStructure.model.SchedualStructurePeriod;
import com.system.modules.schedualStructure.model.Section;

public interface CoreDao extends BaseDao{
	
	public List<Category> findAllCategories();

	public List<Category> getContainerCategories(Long containerId);
	
	void deleteUserContainerRolesByContainerId(Long containerId);

	void deleteUserContainerByContainerId(Long containerId);
	
	void deleteUserContainerRolesByContainerIdAndUserId(Long containerId, Long userId);

	void deleteUserContainerByContainerIdAndUserId(Long containerId, Long userId);

	UserContainer getUserContainer(long containerId, long userId);

	List<UserContainer> getAllUserContainerRelatedToUser(long userId);

	void createUserContainerRole(Long userContainerId, Integer containerRoleId);
	
	public Long checkCountDuplicationOfValueInUserTable(String columnName,  String  value);

	void deleteUserContainerRolesByUserId(long userId);

	void deleteUserContainerByUserId(long userId);

	public List<UserContainerRole> getUserContainerRolesByUserID(Long userId);

	public List<ContainerType> getContainerTypesHierarchyForUser(Long loggedInUserId);

//	public List<Container> childrenContainersHierarchyForContainer(Long containerId);

	public List<User> getContainerUsers(long containerId);

	public ContainerRole getContainerRolesByContainerIdAndRoleId(long containerId, long roleId);
	
	public List<ContainerRole> getContainerRolesByContainerId(long containerId);

	public ClassRoom getClassRoomById(long classRoomId);

	public List<UserContainer> getAllUserContainerByContainer(ClassRoom classRoom);

	public List<Course> getContainerCourses(Long conId);

	public Section getCourseSection(long classRoomId, long schedualStructureId, long courseId);

	public List<ContainerGoals> getContainerGoals(long contId);

	public void deletePreviousContainerContents(long containerId);

	public <T> T getContainerById(Long containerId, Class<T> calzz);

	public List<ContainerTableOfContents> getcontainerContents(long containerId);

	public List<Section> getPeriodSections(long classRoomId, long schedualStructurePeriodId);

	public void deleteContainerTableOfContents(Container container);

	public List<ContainerTableOfContentsWithContainer> getContainerAssignedTableOfContents(Section section);

	public List<Container> getContainerChildsFirstLevel(Long id, ContainerClass containerClass);

	List<ContainerType> getContainerTypeRelatedToSpecificContainer(Long containerId);

	public List<ContainerType> getAllSystemInstanceTypes();

	List<UserContainer> getUserContainersByUserId(long userId);

	public List<ContainerRole> getContainerRolesForUser(long userId);

	ClassRoomSection getClassRoomSection(ClassRoom classRoom, SchedualStructurePeriod schedualStructurePeriod,
			Section section);

}
