package com.system.modules.core.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.system.modules.common.dao.BaseDaoImpl;
import com.system.modules.core.enums.ContainerClass;
import com.system.modules.core.model.Category;
import com.system.modules.core.model.ClassRoom;
import com.system.modules.core.model.Container;
import com.system.modules.core.model.ContainerGoals;
import com.system.modules.core.model.ContainerRole;
import com.system.modules.core.model.ContainerTableOfContents;
import com.system.modules.core.model.ContainerTableOfContentsWithContainer;
import com.system.modules.core.model.ContainerType;
import com.system.modules.core.model.Course;
import com.system.modules.core.model.User;
import com.system.modules.core.model.UserContainer;
import com.system.modules.core.model.UserContainerRole;
import com.system.modules.schedualStructure.model.ClassRoomSection;
import com.system.modules.schedualStructure.model.SchedualStructurePeriod;
import com.system.modules.schedualStructure.model.Section;

@Repository("coreDao")
public class CoreDaoImpl extends BaseDaoImpl implements CoreDao{
	
	public List<Category> findAllCategories() {
		Criteria criteria = getSession().createCriteria(Category.class);
		return criteria.list();
	}
	
	@Override
	public List<Category> getContainerCategories(Long containerId) {
		String sql = "select cat.* from Category cat join ContainerCategory stc on cat.id = stc.category_id where stc.container_id = "
				+ containerId;
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.addEntity(Category.class);
		List<Category> categories = sqlQuery.list();
		if (categories.isEmpty())
			return null;
		return categories;
	}

	@Override
	public void deleteUserContainerRolesByContainerId(Long containerId){
		String sql = "delete usr from UserContainerRole usr "
				+ "join UserContainer us on us.id=usr.userContainer_id where us.container_id="+containerId;
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.executeUpdate();
	}
	
	@Override
	public void deleteUserContainerRolesByContainerIdAndUserId(Long containerId, Long userId) {
		String sql = "delete usr from UserContainerRole usr "
				+ "join UserContainer us on us.id=usr.userContainer_id "
				+ "where us.container_id = "+containerId+" and us.user_id = "+userId;
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.executeUpdate();
	}
	
	@Override
	public void deleteUserContainerByContainerId(Long containerId){
		String sql = "delete us from UserContainer us where us.container_id = "+containerId;
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.executeUpdate();
	}
	
	@Override
	public void deleteUserContainerByContainerIdAndUserId(Long containerId, Long userId) {
		String sql = "delete us from UserContainer us where us.container_id = "+containerId+" and us.user_id = "+userId;
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.executeUpdate();
	}
	
	@Override
	public List<Container> getContainerChildsFirstLevel(Long id , ContainerClass containerClass) {
		String sql = "select * from Container where parentContainer_id = "+id;
		if(containerClass != null)
			sql += " and DTYPE like '"+containerClass+"'";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addEntity(Container.class);
		return query.list();
	}
	
	@Override
	public UserContainer getUserContainer(long containerId, long userId) {
		Criteria criteria = getSession().createCriteria(UserContainer.class);
		criteria.add(Restrictions.eq("container.id", containerId));
		criteria.add(Restrictions.eq("user.id", userId));
		return (UserContainer) criteria.uniqueResult();
	}
	
	@Override
	public ClassRoomSection getClassRoomSection(ClassRoom classRoom, SchedualStructurePeriod schedualStructurePeriod, Section section) {
		Criteria criteria = getSession().createCriteria(ClassRoomSection.class);
		criteria.add(Restrictions.eq("classRoom", classRoom));
		criteria.add(Restrictions.eq("period", schedualStructurePeriod));
		criteria.add(Restrictions.eq("section", section));
		return (ClassRoomSection) criteria.uniqueResult();
	}

	@Override
	public List<UserContainer> getAllUserContainerRelatedToUser(long userId) {
		String sql = "SELECT uc.* FROM UserContainer uc where user_id = " + userId;
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.addEntity(UserContainer.class);
		return sqlQuery.list();
	}
	
	@Override
	public List<ContainerTableOfContentsWithContainer> getContainerAssignedTableOfContents(Section section) {
		Criteria criteria = getSession().createCriteria(ContainerTableOfContentsWithContainer.class);
		criteria.add(Restrictions.eq("assignedContainer", section));
		return criteria.list();
	}
	
	@Override
	public List<Section> getPeriodSections(long classRoomId, long schedualStructurePeriodId) {
		String sql = "SELECT sec.*,con.* from Section sec "
					+"join Container con on con.id=sec.id "
					+"join ClassRoomPeriodsRelationToSections cps on cps.section_id=sec.id "
					+"where cps.classRoom_id = "+classRoomId+" and cps.period_id = "+schedualStructurePeriodId;
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addEntity(Section.class);
		return query.list();
	}
	
	@Override
	public void deleteContainerTableOfContents(Container container) {
		Query query = getSession().createQuery("delete from ContainerTableOfContentsWithContainer where assignedContainer_id = :ID");
		query.setParameter("ID", container.getId());
		query.executeUpdate();
	}
	
	@Override
	public void createUserContainerRole(Long userContainerId, Integer containerRoleId) {
		String sql = "insert into UserContainerRole (userContainer_id,containerRole_id) values("+userContainerId+","+containerRoleId+")";
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.executeUpdate();
	}
	
	@Override
	public Section getCourseSection(long classRoomId, long schedualStructureId, long courseId) {
		String sql = "SELECT con.* from Container con "
				+ "join ClassRoomPeriodsRelationToSections cps on con.id=cps.section_id "
				+ "join SchedualStructurePeriod ssp on ssp.id = cps.period_id "
				+ "where ssp.schedualStructure_id = "+schedualStructureId
				+ " and cps.classRoom_id="+classRoomId+" and con.course_id = "+courseId;
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.addEntity(Section.class);
		return (Section) sqlQuery.uniqueResult();
	}
	
	@Override
	public Long checkCountDuplicationOfValueInUserTable(String columnName,  String  value){
		String sql = "select count(id) from User where "+columnName+" like '"+value+"'";
		Query query = getSession().createQuery(sql);
		return (Long)query.uniqueResult();
	}
	
	@Override
	public List<UserContainer> getUserContainersByUserId(long userId){
		String sql = "select * from UserContainer where user_id= "+userId;
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		return sqlQuery.addEntity(UserContainer.class).list();
	}
	
	@Override
	public void deleteUserContainerRolesByUserId(long userId){
		String sql = "delete usr from UserContainerRole usr join UserContainer us on usr.userContainer_id=us.id where us.user_id="+userId;
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.executeUpdate();
	}
	
	@Override
	public void deleteUserContainerByUserId(long userId){
		String sql = "delete us from UserContainer us where us.user_id = "+userId;
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.executeUpdate();
	}
	
	/*@Override
	public List<Container> childrenContainersHierarchyForContainer(Long containerId) {
			String sql ="select C from Container C";
			Query sqlQuery = getSession().createQuery(sql);
			List<Container> result = (List<Container>) sqlQuery.list();
			
			return result;
	}*/
	
	@Override
	public List<ContainerGoals> getContainerGoals(long contId) {
		Criteria criteria = getSession().createCriteria(ContainerGoals.class);
		if(contId != 0)
		criteria.add(Restrictions.eq("container.id", contId));
		return criteria.list();
	}
	
	@Override
	public List<ContainerType> getContainerTypesHierarchyForUser(Long loggedInUserId) {
		List<ContainerType> result = new ArrayList<ContainerType>();
		String sql1 = "SELECT conType.* from ContainerType conType "
				+ "join Container con on con.id = conType.container_id "
				+ "join UserContainer usrCon on con.id=usrCon.container_id "
				+ "where usrCon.user_id="+loggedInUserId;
		
		SQLQuery sqlQuery = getSession().createSQLQuery(sql1);
		List<ContainerType> containerTypes = sqlQuery.addEntity(ContainerType.class).list();
		for (ContainerType containerType : containerTypes) {
			result.add(containerType);
			String sql2 ="SELECT GROUP_CONCAT(lv SEPARATOR ',') "
					+ "FROM ( SELECT @pv\\:=(SELECT GROUP_CONCAT(id SEPARATOR ',') "
					+ "FROM ContainerType WHERE container_id IN (@pv)) AS lv "
					+ "FROM ContainerType JOIN (SELECT @pv\\:="+containerType.getId()+")tmp "
					+ "WHERE container_id IN (@pv)) a;";
			SQLQuery sqlQuery2 = getSession().createSQLQuery(sql2);
			String uniqueResult = (String) sqlQuery2.uniqueResult();
			if (uniqueResult != null) {
				StringTokenizer ids = new StringTokenizer( uniqueResult,",");
				 while (ids.hasMoreTokens()) {  
					 String nextToken = ids.nextToken();
					 result.add(getObjectById(Long.valueOf(nextToken), ContainerType.class));
			     }
			}
			 
		}
		// To remove duplicated containers
		Set<ContainerType> hs = new HashSet<>();
		hs.addAll(result);
		result.clear();
		result.addAll(hs);
		return result;
	}
	
	@Override
	public List<ContainerType> getContainerTypeRelatedToSpecificContainer(Long containerId) {
		Criteria criteria = getSession().createCriteria(ContainerType.class);
		criteria.add(Restrictions.eq("parentContainer.id", containerId));
		return criteria.list();
	}
	
	@Override
	public ClassRoom getClassRoomById(long classRoomId) {
		ClassRoom classRoom = getObjectById(classRoomId, ClassRoom.class);
		
		Criteria criteria1 = getSession().createCriteria(UserContainer.class);
		criteria1.add(Restrictions.eq("container.id", classRoom.getId()));
		List<UserContainer> userContainers = criteria1.list();
		
		for (UserContainer userContainer : userContainers) {
			Criteria criteria2 = getSession().createCriteria(UserContainerRole.class);
			criteria2.add(Restrictions.eq("userContainer.id", userContainer.getId()));
			List<UserContainerRole> userContainerRoles = criteria2.list();
			for (UserContainerRole userContainerRole : userContainerRoles) {
				userContainer.getUser().getContainerRoles().add(userContainerRole.getContainerRole());
			}
			classRoom.getUsers().add(userContainer.getUser());
		}
		return classRoom;
	}
	
	@Override
	public List<UserContainer> getAllUserContainerByContainer(ClassRoom classRoom) {
		Criteria criteria = getSession().createCriteria(UserContainer.class);
		criteria.add(Restrictions.eq("container.id", classRoom.getId()));
		return criteria.list();
	}
	
	@Override
	public void deletePreviousContainerContents(long containerId) {
		  String hql = "delete from ContainerTableOfContents where container_id= :cid";
		  Query query = getSession().createQuery(hql);
		  query.setLong("cid", containerId);
		  query.executeUpdate();
	}
	
	@Override
	public List<ContainerTableOfContents> getcontainerContents(long containerId) {
		Criteria criteria = getSession().createCriteria(ContainerTableOfContents.class);
		criteria.add(Restrictions.eq("container.id", containerId));
		return criteria.list();
	}
	
	@Override
	public List<User> getContainerUsers(long containerId) {
		List<User> users = new ArrayList<User>();
		String sql = "SELECT * FROM User  us JOIN UserContainer uscon on us.id = uscon.user_id "
				+ "WHERE uscon.container_id = ?";
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.setParameter(0, containerId);
		sqlQuery.addEntity(User.class);
		users = sqlQuery.list();
		String sql2 = "SELECT * FROM ContainerRole cr "
				+ "JOIN UserContainerRole ucr on cr.id = ucr.containerRole_id "
				+ "join UserContainer uc on uc.id = ucr.userContainer_id "
				+ "where uc.user_id = ?  and  uc.container_id = "+containerId;
		SQLQuery sqlQuery2 = getSession().createSQLQuery(sql2);
		sqlQuery2.addEntity(ContainerRole.class);
		for (User user : users) {
			sqlQuery2.setParameter(0, user.getId());
			user.setContainerRoles(sqlQuery2.list());
		}
		return users;
	}
	
	@Override
	public <T> T getContainerById(Long containerId, Class<T> calzz) {
		Criteria criteria = getSession().createCriteria(calzz);
		criteria.add(Restrictions.eq("id", containerId));
		return (T) criteria.uniqueResult();
	}
	
	/*@Override
	public List<Section> getPeriodSections(long classRoomId, long periodId) {
		String sql = "SELECT * from Container "
				+ "con join Section sec on con.id=sec.id "
				+ "where sec.period_id = "+periodId+" and sec.classRoom_id = "+classRoomId;
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addEntity(Section.class);
		return query.list();
	}*/
	
	@Override
	public List<ContainerType> getAllSystemInstanceTypes() {
		String sql = "SELECT ct.* FROM ContainerType ct "
					+"join Container con on ct.container_id = con.id "
					+"WHERE con.DTYPE LIKE 'SystemInstance' ";
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.addEntity(ContainerType.class);
		return sqlQuery.list();
	}
	
	@Override
	public ContainerRole getContainerRolesByContainerIdAndRoleId(long containerId, long roleId) {
		String sql = "SELECT * FROM ContainerRole cr JOIN Role rl on cr.role_id = rl.id "
					+"WHERE cr.container_id=? and cr.role_id=?";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.setParameter(0, containerId);
		query.setParameter(1, roleId);
		query.addEntity(ContainerRole.class);
		return (ContainerRole) query.uniqueResult();
	}
	
	@Override
	public List<ContainerRole> getContainerRolesForUser(long userId) {
		String sql = "SELECT cr.* FROM ContainerRole cr "
					+"join UserContainerRole ucr on ucr.containerRole_id = cr.id "
					+"join UserContainer uc on uc.id = ucr.userContainer_id " 
					+"where uc.user_id = ?";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.setParameter(0, userId);
		query.addEntity(ContainerRole.class);
		return query.list();
	}
	
	@Override
	public List<ContainerRole> getContainerRolesByContainerId(long containerId) {
		String sql = "SELECT * FROM ContainerRole cr JOIN Role rl on cr.role_id = rl.id WHERE cr.container_id=?";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.setParameter(0, containerId);
		query.addEntity(ContainerRole.class);
		return query.list();
	}
	
	@Override
	public List<Course> getContainerCourses(Long conId) {
		Criteria criteria = getSession().createCriteria(Course.class);
		criteria.add(Restrictions.eq("parentContainer.id", conId));
		return criteria.list();
	}

	@Override
	public List<UserContainerRole> getUserContainerRolesByUserID(Long userId) {
		String sql = "SELECT ucr.containerRole_id FROM UserContainerRole ucr "
				+ "join UserContainer uc on uc.id=ucr.userContainer_id " + "where uc.user_id = " + userId;
		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		return sqlQuery.list();
	}
	
}
