package com.system.modules.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.system.modules.common.model.BaseEntity;

@Entity
public class ContainerType extends BaseEntity {

	private static final long serialVersionUID = -5822181574414784724L;
	
	@NotNull(message = "Name is required")
	@Size(min = 1, message = "Name is required")
	@Column(unique = true)
	private String name;
	
	private String description;
	
	@ManyToOne
//	@JsonBackReference
	private Container parentContainer;
	
	/*@Transient
	private Container jsonCont = new Container();*/
	
	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

	public Container getParentContainer() {
		return parentContainer;
	}

	public void setParentContainer(Container parentContainer) {
		this.parentContainer = parentContainer;
	}

	/*public Container getJsonCont() {
		if (container != null) {
			jsonCont.setId(container.getId());
			jsonCont.setName(container.getName());
		}
		return jsonCont;
	}

	public void setJsonCont(Container jsonCont) {
		this.jsonCont = jsonCont;
	}*/

}
