package com.system.modules.core.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.system.modules.common.model.BaseEntity;
@Entity
public class Menu extends BaseEntity{

	private static final long serialVersionUID = -7730293966015597624L;

	@NotNull(message = "Name is required")
	@Size(min = 1, message = "Name is required")
	@Column(unique = true)
	private String name;

	@NotNull(message = "Name is required")
	@Size(min = 1, message = "URL is required")
	@Column(unique = false)
	private String url;

	
	
    @ManyToOne
    @JoinColumn(name="parentId")
    private Menu parentMenu;
 
    @OneToMany(mappedBy="parentMenu")
    private Set<Menu> subordinates = new HashSet<Menu>();
    
    @Column
    @NotNull(message = "isLeaf is required")
	private boolean isLeaf;
    
    @Column
	@NotNull(message = "Priviledge is required")
	private long featurePrivilage;

    @Column
    @NotNull(message = "type is required")
    private int type;

	@ManyToOne(fetch = FetchType.LAZY)
	private Module module;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Menu getParentMenu() {
		return parentMenu;
	}

	public void setParentMenu(Menu parentMenu) {
		this.parentMenu = parentMenu;
	}

	public Set<Menu> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(Set<Menu> subordinates) {
		this.subordinates = subordinates;
	}

	public boolean isLeaf() {
		return isLeaf;
	}

	public void setLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}

	public long getFeaturePrivilage() {
		return featurePrivilage;
	}

	public void setFeaturePrivilage(long featurePrivilage) {
		this.featurePrivilage = featurePrivilage;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

}
