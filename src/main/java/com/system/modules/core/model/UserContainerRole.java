package com.system.modules.core.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.system.modules.common.model.BaseEntity;

@Entity
public class UserContainerRole extends BaseEntity {

	private static final long serialVersionUID = 3511005785614393463L;
	
	@ManyToOne
	protected UserContainer userContainer;
	@ManyToOne
	private ContainerRole containerRole;

	public void setUserContainer(UserContainer userContainer) {
		this.userContainer = userContainer;
	}
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="id", scope=UserContainer.class)
	public UserContainer getUserContainer() {
		return userContainer;
	}

	public ContainerRole getContainerRole() {
		return containerRole;
	}

	public void setContainerRole(ContainerRole containerRole) {
		this.containerRole = containerRole;
	}

}
