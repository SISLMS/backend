package com.system.modules.core.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.system.modules.common.model.BaseEntity;

@Entity
public class ContainerGoals extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1081947734344207281L;

	private String name;
	@ManyToOne
	private Container container;
	@ManyToOne
	private ContainerGoals parentGoal;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public ContainerGoals getParentGoal() {
		return parentGoal;
	}

	public void setParentGoal(ContainerGoals parentGoal) {
		this.parentGoal = parentGoal;
	}
}
