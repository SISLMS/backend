package com.system.modules.core.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import com.system.modules.common.model.BaseEntity;

@Entity
public class RolePage extends BaseEntity{

	private static final long serialVersionUID = 4998666472313812005L;
	
	@ManyToOne
	private Role role;
	@ManyToOne
	private Page page;
	@ManyToOne
	private Container container;
	@ManyToOne
	private ContainerType containerType;


	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public ContainerType getContainerType() {
		return containerType;
	}

	public void setContainerType(ContainerType containerType) {
		this.containerType = containerType;
	}
}
