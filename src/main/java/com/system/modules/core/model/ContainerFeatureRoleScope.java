package com.system.modules.core.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.system.modules.common.model.BaseEntity;

@Entity
public class ContainerFeatureRoleScope extends BaseEntity{

	private static final long serialVersionUID = 3087785431164403343L;
	
	@ManyToOne
	private Container container;
	@ManyToOne
	private ContainerType containerType;
	@ManyToOne
	private Role role;
	@ManyToOne
	private Feature feature;

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public ContainerType getContainerType() {
		return containerType;
	}

	public void setContainerType(ContainerType containerType) {
		this.containerType = containerType;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Feature getFeature() {
		return feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}
}
