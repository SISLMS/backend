package com.system.modules.core.model;

import javax.persistence.Entity;
import com.system.modules.common.model.BaseEntity;

@Entity
public class Page extends BaseEntity{

	private static final long serialVersionUID = 2816850460978369334L;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
