package com.system.modules.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.system.modules.common.model.BaseEntity;

@Entity
public class Component extends BaseEntity {

	private static final long serialVersionUID = -3481834750049388232L;

	@NotNull(message = "Name is required")
	@Size(min = 1, message = "Name is required")
	@Column(unique = true)
	private String name;

	private String url;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
