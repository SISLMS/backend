package com.system.modules.core.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.system.modules.common.model.BaseEntity;

@Entity
public class ContainerTableOfContents extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 4403223163168904847L;
	
	private Long parent;
	
	private String name;
	
	@ManyToOne
	private Container container;
	
	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public Long getParent() {
		return parent;
	}

	public void setParent(Long parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
