package com.system.modules.core.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.system.modules.common.model.BaseEntity;

@Entity
public class ContainerCategory extends BaseEntity{

	private static final long serialVersionUID = 8092035329499211027L;
	
	@ManyToOne
	private Container container;
	@ManyToOne
	private Category category;
	
	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}
