package com.system.modules.core.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class ClassRoom extends Container{

	private static final long serialVersionUID = 7046735545153963861L;
	
	@Transient
	private Set<User> users = new HashSet<User>();
	
	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	

}
