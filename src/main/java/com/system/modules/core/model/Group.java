package com.system.modules.core.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.system.modules.common.model.BaseEntity;

@Entity
public class Group extends BaseEntity{

	private static final long serialVersionUID = -5475425024309206202L;
	
	private String name;
	@ManyToOne
	private GroupScope groupScope;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GroupScope getGroupScope() {
		return groupScope;
	}

	public void setGroupScope(GroupScope groupScope) {
		this.groupScope = groupScope;
	}

}
