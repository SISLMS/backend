package com.system.modules.core.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.system.modules.common.model.BaseEntity;

@Entity
public class ContainerRole extends BaseEntity {

	private static final long serialVersionUID = -7384453654801356384L;

	@NotNull(message = "Name is required")
	private int allowedPriviledges;

	private int moduleId;

	@ManyToOne
	private Role role;

	@ManyToOne
	private Container container;

	public void setContainer(Container container) {
		this.container = container;
	}

	public Container getContainer() {
		return container;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Role getRole() {
		return role;
	}

	public void setAllowedPriviledges(int allowedPriviledges) {
		this.allowedPriviledges = allowedPriviledges;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public int getAllowedPriviledges() {
		return allowedPriviledges;
	}

	public int getModuleId() {
		return moduleId;
	}

}
