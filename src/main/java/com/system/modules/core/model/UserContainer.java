package com.system.modules.core.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.system.modules.common.model.BaseEntity;

@Entity
public class UserContainer extends BaseEntity {
	
	private static final long serialVersionUID = 7876661399998116045L;

	@ManyToOne
	protected Container container;
	
	@ManyToOne
	protected User user;
	
	public void setContainer(Container container) {
		this.container = container;
	}

	public Container getContainer() {
		return container;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

}
