package com.system.modules.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.system.modules.common.model.BaseEntity;

@Entity
public class Feature extends BaseEntity{

	private static final long serialVersionUID = -7401264275629775392L;

	@NotNull(message = "Name is required")
	@Size(min = 1, message = "Name is required")
	@Column(unique = true)
	protected String name;

	@NotNull(message = "Description Name is required")
	@Size(min = 1, message = "DescriptionName is required")
	@Column(unique = true)
	protected String description;

	protected int moduleId;

	@NotNull(message = "Priviledge Name is required")
	protected int priviledge;

	@ManyToOne(fetch = FetchType.LAZY)
	protected ContainerType containerType;

	
	
	public void setDescription(String description) {
		this.description = description;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPriviledge(int priviledge) {
		this.priviledge = priviledge;
	}

	public String getDescription() {
		return description;
	}

	public int getModuleId() {
		return moduleId;
	}

	public String getName() {
		return name;
	}

	public int getPriviledge() {
		return priviledge;
	}

	public void setContainerType(ContainerType containerType) {
		this.containerType = containerType;
	}

	public ContainerType getContainerType() {
		return containerType;
	}

}
