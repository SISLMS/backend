package com.system.modules.core.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.system.modules.common.model.BaseEntity;

@Entity
public class ContainerTableOfContentsWithContainer extends BaseEntity{

	private static final long serialVersionUID = -2364412776444027925L;
	@ManyToOne
	private Container assignedContainer;
	
	@ManyToOne
	private ContainerTableOfContents containerTableOfContent;

	public Container getAssignedContainer() {
		return assignedContainer;
	}

	public void setAssignedContainer(Container assignedContainer) {
		this.assignedContainer = assignedContainer;
	}

	public ContainerTableOfContents getContainerTableOfContent() {
		return containerTableOfContent;
	}

	public void setContainerTableOfContent(ContainerTableOfContents containerTableOfContent) {
		this.containerTableOfContent = containerTableOfContent;
	}
}
