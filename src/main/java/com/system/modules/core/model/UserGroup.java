package com.system.modules.core.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.system.modules.common.model.BaseEntity;

@Entity
public class UserGroup extends BaseEntity{

	private static final long serialVersionUID = -6064252684554834659L;
	
	@ManyToOne
	private Group group;
	@ManyToOne
	private User user;

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
