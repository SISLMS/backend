package com.system.modules.core.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.system.modules.common.model.BaseEntity;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE) 
public class Container extends BaseEntity {

	private static final long serialVersionUID = 7607477127891397345L;

	@NotNull(message = "Name is required")
	@Size(min = 1, message = "Name is required")
	@Column(unique = true)
	private String name;
	
	private Date startDate;

	private Date endDate;

	private Boolean suspended;

	@Column(columnDefinition="TEXT")
	private String address;

	@ManyToOne
	private ContainerType containerType;
	
	@ManyToOne
	private Container parentContainer;
	
	@JsonIgnore
	@Transient
	private List<Container> childContainers = new ArrayList<Container>();
	
	
	public List<Container> getChildContainers() {
		return childContainers;
	}

	public void setChildContainers(List<Container> childContainers) {
		this.childContainers = childContainers;
	}

	public void setContainerType(ContainerType containerType) {
		this.containerType = containerType;
	}

	public ContainerType getContainerType() {
		return containerType;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setSuspended(Boolean suspended) {
		this.suspended = suspended;
	}

	public String getAddress() {
		return address;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndDate() {
		return endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Boolean getSuspended() {
		return suspended;
	}

	public Container getParentContainer() {
		return parentContainer;
	}

	public void setParentContainer(Container parentContainer) {
		this.parentContainer = parentContainer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
