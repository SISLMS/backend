package com.system.modules.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.system.modules.common.model.BaseEntity;

@Entity
public class Role extends BaseEntity {

	private static final long serialVersionUID = -3651215806656025770L;

	@NotNull(message = "Name is required")
	@Size(min = 1, message = "Name is required")
	@Column(unique = true)
	private String name;
	private String description;

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}
}
