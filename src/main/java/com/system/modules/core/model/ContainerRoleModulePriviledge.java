package com.system.modules.core.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.system.modules.common.model.BaseEntity;

@Entity
public class ContainerRoleModulePriviledge extends BaseEntity{

	private static final long serialVersionUID = 3390807767351519404L;
	
	@ManyToOne
	private Container container;
	@ManyToOne
	private Role role;
	@ManyToOne
	private Module module;
	private byte[] featurePriviledge;

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public byte[] getFeaturePriviledge() {
		return featurePriviledge;
	}

	public void setFeaturePriviledge(byte[] featurePriviledge) {
		this.featurePriviledge = featurePriviledge;
	}
}
