package com.system.modules.core.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.system.modules.common.model.BaseEntity;

@Entity
public class ObjectType extends BaseEntity {

	private static final long serialVersionUID = 3384980192942608203L;
	
	private String name;
	@ManyToOne
	private Module module;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}
}
