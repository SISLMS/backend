package com.system.modules.core.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.system.modules.common.model.BaseEntity;

@Entity
public class UserContainerLastVisit extends BaseEntity {

	private static final long serialVersionUID = -4339250583809475459L;
	
	@ManyToOne
	private UserContainer userContainer;
	private Date loginDate;
	private String clientInfo;

	public UserContainer getUserContainer() {
		return userContainer;
	}

	public void setUserContainer(UserContainer userContainer) {
		this.userContainer = userContainer;
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public String getClientInfo() {
		return clientInfo;
	}

	public void setClientInfo(String clientInfo) {
		this.clientInfo = clientInfo;
	}
}
