package com.system.modules.core.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.system.modules.common.model.BaseEntity;

@Entity
public class ContainerModule extends BaseEntity{

	private static final long serialVersionUID = -6179791520231780992L;
	
	@ManyToOne
	private Container container;
	@ManyToOne
	private ContainerType containerType;
	@ManyToOne
	private Module module;
	private byte[] featurePriviledge;

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public ContainerType getContainerType() {
		return containerType;
	}

	public void setContainerType(ContainerType containerType) {
		this.containerType = containerType;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public byte[] getFeaturePriviledge() {
		return featurePriviledge;
	}

	public void setFeaturePriviledge(byte[] featurePriviledge) {
		this.featurePriviledge = featurePriviledge;
	}
}
