package com.system.modules.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.system.modules.common.model.BaseEntity;

@Entity
public class Module extends BaseEntity{
	
	private static final long serialVersionUID = 6525804748815435630L;
	
	@NotNull(message = "Name is required")
	@Size(min = 1, message = "Name is required")
	@Column(unique = true)
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
