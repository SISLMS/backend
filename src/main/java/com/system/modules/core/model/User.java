package com.system.modules.core.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.system.modules.common.enums.Gender;
import com.system.modules.common.model.BaseEntity;
import com.system.modules.core.dto.ContainerDTO;


@Entity
public class User extends BaseEntity {

	private static final long serialVersionUID = 3823432608524645250L;

	@NotNull(message = "User Name is required")
	@Size(min = 1, message = "User Name is required")
	@Column(unique = true)
	protected String userName;

	@NotNull(message = "Password is required")
	@Size(min = 1, message = "Password is required")
	protected String password;

	@NotNull(message = "Name is required")
	@Size(min = 1, message = "Name is required")
	protected String name;

	@NotNull(message = "Suspended is required")
	protected Boolean suspended;
	
	protected String address;
	
	protected String email;
	@Enumerated(EnumType.ORDINAL)
	private Gender gender;
	private Date birthDate;
	protected Date lastModifiedDate;
	
	@Transient
	private List<ContainerRole> containerRoles = new ArrayList<ContainerRole>();
	@Transient
	private List<ContainerDTO> courses = new ArrayList<ContainerDTO>();
	

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setSuspended(Boolean suspended) {
		this.suspended = suspended;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public Boolean getSuspended() {
		return (suspended != null)?suspended:false;
	}

	public String getUserName() {
		return userName;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getEmail() {
		return email;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public List<ContainerRole> getContainerRoles() {
		return containerRoles;
	}

	public void setContainerRoles(List<ContainerRole> containerRoles) {
		this.containerRoles = containerRoles;
	}

	public List<ContainerDTO> getCourses() {
		return courses;
	}

	public void setCourses(List<ContainerDTO> courses) {
		this.courses = courses;
	}


}
