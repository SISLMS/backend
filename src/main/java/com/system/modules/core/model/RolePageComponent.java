package com.system.modules.core.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import com.system.modules.common.model.BaseEntity;

@Entity
public class RolePageComponent extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3335974833549348189L;
	@ManyToOne
	private RolePage rolePage;
	@ManyToOne
	private Component component;
	private int xPos;
	private int yPos;


	public RolePage getRolePage() {
		return rolePage;
	}

	public void setRolePage(RolePage rolePage) {
		this.rolePage = rolePage;
	}

	public Component getComponent() {
		return component;
	}

	public void setComponent(Component component) {
		this.component = component;
	}

	public int getxPos() {
		return xPos;
	}

	public void setxPos(int xPos) {
		this.xPos = xPos;
	}

	public int getyPos() {
		return yPos;
	}

	public void setyPos(int yPos) {
		this.yPos = yPos;
	}
}
