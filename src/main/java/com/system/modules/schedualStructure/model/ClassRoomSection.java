package com.system.modules.schedualStructure.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.system.modules.common.model.BaseEntity;
import com.system.modules.core.model.ClassRoom;

@Entity
public class ClassRoomSection extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 4095742606737428475L;

	@ManyToOne
	private SchedualStructurePeriod period;
	@ManyToOne
	private Section section;
	@ManyToOne
	private ClassRoom classRoom;

	public SchedualStructurePeriod getPeriod() {
		return period;
	}

	public void setPeriod(SchedualStructurePeriod period) {
		this.period = period;
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public ClassRoom getClassRoom() {
		return classRoom;
	}

	public void setClassRoom(ClassRoom classRoom) {
		this.classRoom = classRoom;
	}
}
