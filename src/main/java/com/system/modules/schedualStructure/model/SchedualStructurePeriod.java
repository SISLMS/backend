package com.system.modules.schedualStructure.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.system.modules.common.enums.Days;
import com.system.modules.common.model.BaseEntity;

@Entity
public class SchedualStructurePeriod extends BaseEntity {

	
	private static final long serialVersionUID = 4300654688428065112L;

	private Days day;
	
	@ManyToOne
	private SchedualStructure schedualStructure;
	
	@ManyToOne
	private SchedualPeriodType schedualPeriodType;
	
	private String name;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	

	public SchedualPeriodType getSchedualPeriodType() {
		return schedualPeriodType;
	}

	public void setSchedualPeriodType(SchedualPeriodType schedualPeriodType) {
		this.schedualPeriodType = schedualPeriodType;
	}

	public Days getDay() {
		return day;
	}

	public void setDay(Days day) {
		this.day = day;
	}

	public SchedualStructure getSchedualStructure() {
		return schedualStructure;
	}

	public void setSchedualStructure(SchedualStructure schedualStructure) {
		this.schedualStructure = schedualStructure;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
