package com.system.modules.schedualStructure.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.system.modules.common.enums.Days;
import com.system.modules.common.model.BaseEntity;

@Entity
public class SchedualStructurePeriodInstance extends BaseEntity {

	private static final long serialVersionUID = 630628833996914839L;

	@ManyToOne
	private SchedualStructurePeriod period;

	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;

	@Transient
	private SchedualPeriodType periodType;
	
	@Transient
	private Days day;
	
	@Transient
	private SchedualStructure schedualStructure;
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Days getDay() {
		return day;
	}

	public void setDay(Days day) {
		this.day = day;
	}

	public SchedualStructurePeriod getPeriod() {
		return period;
	}

	public void setPeriod(SchedualStructurePeriod period) {
		this.period = period;
	}

	public SchedualPeriodType getPeriodType() {
		return periodType;
	}

	public void setPeriodType(SchedualPeriodType periodType) {
		this.periodType = periodType;
	}

	public SchedualStructure getSchedualStructure() {
		return schedualStructure;
	}

	public void setSchedualStructure(SchedualStructure schedualStructure) {
		this.schedualStructure = schedualStructure;
	}

}
