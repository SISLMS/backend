package com.system.modules.schedualStructure.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import com.system.modules.common.model.BaseEntity;
import com.system.modules.core.model.Container;

@Entity
public class SchedualStructure extends BaseEntity {

	private static final long serialVersionUID = 7070216666975774882L;

	@ManyToOne
	private Container container;

	@Column(unique = true)
	private String name;


	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
