package com.system.modules.schedualStructure.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import com.system.modules.core.model.Container;
import com.system.modules.core.model.Course;
import com.system.modules.core.model.User;

@Entity
public class Section extends Container implements Serializable {

	private static final long serialVersionUID = -6517397548825900125L;

	@ManyToOne
	private Course course;
	
	@Transient
	private List<User> sectionUsers = new ArrayList<User>();

	public List<User> getSectionUsers() {
		return sectionUsers;
	}

	public void setSectionUsers(List<User> sectionUsers) {
		this.sectionUsers = sectionUsers;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

}
