package com.system.modules.schedualStructure.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.system.modules.common.Utils.LMSUtil;
import com.system.modules.core.model.ClassRoom;
import com.system.modules.schedualStructure.model.SchedualPeriodType;
import com.system.modules.schedualStructure.model.SchedualStructure;
import com.system.modules.schedualStructure.model.SchedualStructurePeriod;
import com.system.modules.schedualStructure.model.SchedualStructurePeriodInstance;
import com.system.modules.schedualStructure.service.SchedualStructureService;

@RestController
@RequestMapping("/api/schedualStructureController")
@Transactional
public class ScheduleStructureController {
	
	@Autowired
	SchedualStructureService schedualStructureService;
	
	//--------------------------------- Create a New Schedule Structure ------------------------------------------
    
    @RequestMapping(value = "/newSchedualStructure/", method = RequestMethod.POST)
    public void createNewScheduleStructure(@RequestBody SchedualStructure schedualStructure) {
    	schedualStructureService.saveOrUpdate(schedualStructure);
    }
    
    //--------------------------------- Create a New Schedule Structure Period ------------------------------------------
    
    @RequestMapping(value = "/newSchedualStructurePeriod/", method = RequestMethod.POST)
    public void createScheduleStructurePeriod(@RequestBody SchedualStructurePeriod schedualStructurePeriod) {
    	schedualStructureService.createcheduleStructurePeriod(schedualStructurePeriod);
    }	
    
    //--------------------------------- Update Schedule Structure Periods ------------------------------------------
    
    @RequestMapping(value = "/updateSchedualStructurePeriod/", method = RequestMethod.POST)
    public void updateSchedualStructurePeriod(@RequestBody SchedualStructurePeriod schedualStructurePeriod) {
    	schedualStructureService.updateSchedualStructurePeriod(schedualStructurePeriod);
    }	
    
    @RequestMapping(value = "/deleteSchedualStructurePeriod/", method = RequestMethod.POST)
    public void deleteSchedualStructurePeriod(@RequestBody SchedualStructurePeriod schedualStructurePeriod) {
    	schedualStructureService.deleteSchedualStructurePeriod(schedualStructurePeriod);
    }	
    
    @RequestMapping(value = "/getSchedualStructureBySSID/{id}", method = RequestMethod.GET)
    public ResponseEntity<SchedualStructure> getSchedualStructure(@PathVariable("id") long sSId) {
    	SchedualStructure schedualStructure = schedualStructureService.getObjectById(sSId, SchedualStructure.class);
    	return new ResponseEntity<SchedualStructure>(schedualStructure, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getAllSchedualStructures", method = RequestMethod.GET)
    public ResponseEntity<List<SchedualStructure>> availableSchedualStructure() {
    	List<SchedualStructure> schedualStructures = schedualStructureService.getAllSchedualStructures();
    	if (!schedualStructures.isEmpty()) {
    		return new ResponseEntity<List<SchedualStructure>>(schedualStructures, HttpStatus.OK);
		}
    	return null;
    }
    
    @RequestMapping(value = "/getSchedualStructurePeriodsRelatedToContainer", method = RequestMethod.GET)
    public ResponseEntity<List<SchedualStructurePeriod>> getSchedualStructurePeriodsRelatedToContainer(@RequestParam long schedualStructureID) {
    	List<SchedualStructurePeriod> schedualStructurePeriods = schedualStructureService.getSchedualStructurePeriodsBySSID(schedualStructureID);
    	if (!schedualStructurePeriods.isEmpty()) {
    		return new ResponseEntity<List<SchedualStructurePeriod>>(schedualStructurePeriods, HttpStatus.OK);
		}
    	return null;
    }
    
    @RequestMapping(value = "/getSchedualStructurePeriodsSection", method = RequestMethod.GET)
    public ResponseEntity<List<SchedualStructurePeriodInstance>> getSchedualStructurePeriodsSection(@RequestParam long classRoomId, @RequestParam long schedualStructureID) {
    	List<SchedualStructurePeriodInstance> instances = schedualStructureService.getSchedualStructurePeriodInstancesClassRoom(classRoomId, schedualStructureID);
    	if (!instances.isEmpty()) {
    		return new ResponseEntity<List<SchedualStructurePeriodInstance>>(instances, HttpStatus.OK);
		}
    	return null;
    }
    
    
    @RequestMapping(value = "/getSchedualStructurePeriodsBySSID/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<SchedualStructurePeriod>> getSchedualStructurePeriodsBySSID(@PathVariable("id") long sSId) {
    	List<SchedualStructurePeriod> schedualStructurePeriods = schedualStructureService.getSchedualStructurePeriodsBySSID(sSId);
    	if (!schedualStructurePeriods.isEmpty()) {
    		return new ResponseEntity<List<SchedualStructurePeriod>>(schedualStructurePeriods, HttpStatus.OK);
		}
    	return null;
    }
    
    @RequestMapping(value = "/getSchedualStructurePeriodsToUser/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<SchedualStructurePeriod>> getSchedualStructurePeriodsToUser(@PathVariable("userId") long userId) {
    	List<SchedualStructurePeriod> schedualStructurePeriods = schedualStructureService.getSchedualStructurePeriodsToUser(userId);
    	if (!schedualStructurePeriods.isEmpty()) {
    		return new ResponseEntity<List<SchedualStructurePeriod>>(schedualStructurePeriods, HttpStatus.OK);
		}
    	return null;
    }
    
    @RequestMapping(value = "/getPreviousClassRoomForSchedualStructurePeriod/{SSPID}", method = RequestMethod.GET)
    public ResponseEntity<ClassRoom> getPreviousClassRoomForSchedualStructurePeriod(@PathVariable("SSPID") long SSPID) {
    	ClassRoom classRoom = schedualStructureService.getPreviousClassRoomForSchedualStructurePeriodBySSPID(SSPID);
    	return new ResponseEntity<ClassRoom>(classRoom, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/SchedualPeriodTypes", method = RequestMethod.GET)
    public ResponseEntity<List<SchedualPeriodType>> availableSchedualPeriodType() {
    	List<SchedualPeriodType> schedualPeriodTypes = schedualStructureService.getAllSchedualPeriodTypes();
    	System.out.println("SchedualPeriodTypes");
    	if (!schedualPeriodTypes.isEmpty()) {
    		return new ResponseEntity<List<SchedualPeriodType>>(schedualPeriodTypes, HttpStatus.OK);
		}
    	return null;
    }
    
    @RequestMapping(value = "/createClassRoomAndRelatedContainers/", method = RequestMethod.POST)
    public void createClassRoomAndRelatedContainers(@RequestBody ClassRoom[] ClassRooms) {
    	schedualStructureService.createClassRoomAndRelatedContainers(ClassRooms);
    }
    
    @RequestMapping(value = "/getTermStartAndEnd/", method = RequestMethod.GET)
    public ResponseEntity<List<String>> getPreviousClassRoomForSchedualStructurePeriod() {
    	List<String> result = new ArrayList<String>();
    	result.add(new SimpleDateFormat("yyyy-MM-dd").format(LMSUtil.getTermStart().toDate()).toString());
    	result.add(new SimpleDateFormat("yyyy-MM-dd").format(LMSUtil.getTermEnd().toDate()).toString());
    	return new ResponseEntity<List<String>>(result, HttpStatus.OK);
    }
    
}
