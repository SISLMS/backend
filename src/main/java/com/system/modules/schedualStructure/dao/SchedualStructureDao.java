package com.system.modules.schedualStructure.dao;

import java.util.List;

import org.joda.time.DateTime;

import com.system.modules.common.dao.BaseDao;
import com.system.modules.core.model.ClassRoom;
import com.system.modules.schedualStructure.model.SchedualPeriodType;
import com.system.modules.schedualStructure.model.SchedualStructure;
import com.system.modules.schedualStructure.model.SchedualStructurePeriod;
import com.system.modules.schedualStructure.model.SchedualStructurePeriodInstance;


public interface SchedualStructureDao extends BaseDao {
	
	List<SchedualPeriodType> getAllSchedualPeriodTypes();

	List<SchedualStructure> getAllSchedualStructures();

	List<SchedualStructurePeriod> getSchedualStructurePeriodsBySSID(long sSId);

	void deletePreviousSchedualStructurePeriods(SchedualStructure schedualStructure);

	void createClassRoomAndRelatedContainers(ClassRoom[] classRooms);

	ClassRoom getPreviousClassRoomForSchedualStructurePeriodBySSPID(long sSPID);

	List<SchedualStructurePeriod> getSchedualStructurePeriodsToUser(long userId);

	List<SchedualStructurePeriod> getRelatedPeriodsInSchedual(SchedualStructurePeriod schedualStructurePeriod);

	List<SchedualStructurePeriodInstance> getPeriodInstances(SchedualStructurePeriod schedualStructurePeriod);

	List<SchedualStructurePeriodInstance> getSchedualStructurePeriodInstancesClassRoom(long classRoomId, long schedualStructureID);

}
