package com.system.modules.schedualStructure.dao;

import java.text.SimpleDateFormat;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

import com.system.modules.common.Utils.LMSUtil;
import com.system.modules.common.dao.BaseDaoImpl;
import com.system.modules.core.model.ClassRoom;
import com.system.modules.schedualStructure.model.SchedualPeriodType;
import com.system.modules.schedualStructure.model.SchedualStructure;
import com.system.modules.schedualStructure.model.SchedualStructurePeriod;
import com.system.modules.schedualStructure.model.SchedualStructurePeriodInstance;

@Repository("schedualStructureDao")
public class SchedualStructureDaoImpl extends BaseDaoImpl implements SchedualStructureDao{
	
	@Override
	public List<SchedualPeriodType> getAllSchedualPeriodTypes() {
		Criteria criteria = getSession().createCriteria(SchedualPeriodType.class);
		return criteria.list();
	}
	
	@Override
	public List<SchedualStructure> getAllSchedualStructures() {
		Criteria criteria = getSession().createCriteria(SchedualStructure.class);
		return criteria.list();
	}
	
	@Override
	public List<SchedualStructurePeriod> getRelatedPeriodsInSchedual(SchedualStructurePeriod schedualStructurePeriod) {
		String condition = "";
		DateTime periodStartDate = new DateTime(schedualStructurePeriod.getStartDate());
		DateTime periodEndDate = new DateTime(schedualStructurePeriod.getEndDate());
		DateTime termStart = LMSUtil.getTermStart();
		DateTime termEnd = LMSUtil.getTermEnd();
    	while (periodStartDate.toLocalDate().isBefore(termEnd.toLocalDate())) {
//			if (periodStartDate.toLocalDate().isAfter(termStart.toLocalDate()) || periodStartDate.toLocalDate().isEqual(termStart.toLocalDate())) {
				if (condition.equals("")) condition+=" and ";
				else condition+=" or ";
				String startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(periodStartDate.toDate());
				String endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(periodEndDate.toDate());
				condition +=  "( startDate = '"+startDate+"' and endDate = '"+endDate+"' )";
//			}
    		periodStartDate = periodStartDate.plusWeeks(1);
    		periodEndDate = periodEndDate.plusWeeks(1);
		}
		String sql = "SELECT * FROM SchedualStructurePeriod WHERE schedualStructure_id = "+schedualStructurePeriod.getSchedualStructure().getId();
		sql += condition;
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addEntity(SchedualStructurePeriod.class);
		return query.list();
	}
	
	@Override
	public List<SchedualStructurePeriod> getSchedualStructurePeriodsBySSID(long sSId) {
		Criteria criteria = getSession().createCriteria(SchedualStructurePeriod.class);
		criteria.add(Restrictions.eq("schedualStructure.id", sSId));
		return criteria.list();
	}
	
	@Override
	public List<SchedualStructurePeriod> getSchedualStructurePeriodsToUser(long userId) {
		String sql = "SELECT ssp.* FROM Container con "
				+ "join UserContainer uc on uc.container_id = con.id "
				+ "join SchedualStructurePeriod ssp on ssp.id = con.period_id "
				+ "where uc.user_id = "+userId;
		SQLQuery query = getSession().createSQLQuery(sql);
		return query.addEntity(SchedualStructurePeriod.class).list();
	}
	
	@Override
	public void deletePreviousSchedualStructurePeriods(SchedualStructure schedualStructure) {
		String sql = "DELETE from SchedualStructurePeriod WHERE schedualStructure_id="+schedualStructure.getId();
		SQLQuery query = getSession().createSQLQuery(sql);
		query.executeUpdate();
	}
	
	@Override
	public List<SchedualStructurePeriodInstance> getPeriodInstances(SchedualStructurePeriod schedualStructurePeriod) {
		Criteria criteria = getSession().createCriteria(SchedualStructurePeriodInstance.class);
		criteria.add(Restrictions.eq("period", schedualStructurePeriod));
		return criteria.list();
	}
	
	@Override
	public List<SchedualStructurePeriodInstance> getSchedualStructurePeriodInstancesClassRoom(long classRoomId, long schedualStructureID) {
		String sql = "SELECT * from SchedualStructurePeriodInstance sspi "
					+"join SchedualStructurePeriod ssp on sspi.period_id = ssp.id "
					+"join SchedualStructure ss on ss.id = ssp.schedualStructure_id  "
					+"join ClassRoomPeriodsRelationToSections cps on cps.period_id = ssp.id  "
					+"where cps.classRoom_id = "+classRoomId+" and ss.id ="+schedualStructureID+" GROUP BY sspi.id";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addEntity(SchedualStructurePeriodInstance.class);
		return query.list();
	}
	
	@Override
	public void createClassRoomAndRelatedContainers(ClassRoom[] classRooms) {
		for (ClassRoom classRoom : classRooms) {
			saveOrUpdate(classRoom);
		}
	}

	@Override
	public ClassRoom getPreviousClassRoomForSchedualStructurePeriodBySSPID(long sSPID) {
		String sql = "SELECT * FROM ClassRoom WHERE period_id = :periodId";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.setLong("periodId", sSPID);
		query.addEntity(ClassRoom.class);
		return (ClassRoom) query.uniqueResult();
	}
}
