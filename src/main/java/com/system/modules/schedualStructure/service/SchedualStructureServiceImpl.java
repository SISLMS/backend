package com.system.modules.schedualStructure.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.system.modules.common.Utils.LMSUtil;
import com.system.modules.common.service.BaseServiceImpl;
import com.system.modules.core.model.ClassRoom;
import com.system.modules.schedualStructure.dao.SchedualStructureDao;
import com.system.modules.schedualStructure.model.SchedualPeriodType;
import com.system.modules.schedualStructure.model.SchedualStructure;
import com.system.modules.schedualStructure.model.SchedualStructurePeriod;
import com.system.modules.schedualStructure.model.SchedualStructurePeriodInstance;

@Service("schedualStructureService")
@Transactional
public class SchedualStructureServiceImpl extends BaseServiceImpl implements SchedualStructureService {
	
	@Autowired
	private SchedualStructureDao schedualStructureDao;
	
	
	
	@Override
	public void createcheduleStructurePeriod(SchedualStructurePeriod schedualStructurePeriod) {
		SchedualStructurePeriod period = new SchedualStructurePeriod();
		period.setStartDate(schedualStructurePeriod.getStartDate());
		period.setEndDate(schedualStructurePeriod.getEndDate());
		period.setName(schedualStructurePeriod.getName());
		period.setSchedualPeriodType(schedualStructurePeriod.getSchedualPeriodType());
		period.setDay(schedualStructurePeriod.getDay());
		period.setSchedualStructure(schedualStructurePeriod.getSchedualStructure());
		saveBaseEntity(period);
		createScheduleStructurePeriodInstances(period);
	}
	
	@Override
	public void deleteSchedualStructurePeriod(SchedualStructurePeriod schedualStructurePeriod) {
	}
	
	@Override
	public void updateSchedualStructurePeriod(SchedualStructurePeriod schedualStructurePeriod) {
		saveOrUpdate(schedualStructurePeriod);
	}
	
	@Override
	public void createScheduleStructurePeriodInstances(SchedualStructurePeriod schedualStructurePeriod) {
		
    	DateTime termStart = LMSUtil.getTermStart();
    	DateTime termEnd = LMSUtil.getTermEnd();
    	Integer plusDays = 0;
    	
    	switch (schedualStructurePeriod.getDay().toString().toUpperCase()) {
		case "SATURDAY":
			plusDays = 0;
			break;
		case "SUNDAY":
			plusDays = 1;
			break;
		case "MONDAY":
			plusDays = 2;
			break;
		case "TUESDAY":
			plusDays = 3;
			break;
		case "WEDNESDAY":
			plusDays = 4;
			break;
		case "THURSDAY":
			plusDays = 5;
			break;
		case "FRIDAY":
			plusDays = 6;
			break;
		}
    	
    	DateTime periodStart = termStart.plusDays(plusDays);
    	String periodStartDateString = periodStart.toString().substring(0, termStart.toString().indexOf("T"));
    	
    	String periodStartDateOnly = schedualStructurePeriod.getStartDate().toString();
    	String timeStartOfPeriod = periodStartDateOnly.substring(periodStartDateOnly.indexOf(" ", periodStartDateOnly.indexOf(" ", periodStartDateOnly.indexOf(" ")+1)+1)+1, periodStartDateOnly.indexOf("EET")-1);
    	String periodEndDateOnly = schedualStructurePeriod.getEndDate().toString();
    	String timeEndOfPeriod = periodEndDateOnly.substring(periodEndDateOnly.indexOf(" ", periodEndDateOnly.indexOf(" ", periodEndDateOnly.indexOf(" ")+1)+1)+1, periodEndDateOnly.indexOf("EET")-1);
    	
    		DateTime periodStartDate = new DateTime(periodStartDateString +"T"+ timeStartOfPeriod);
    		DateTime periodEndDate = new DateTime(periodStartDateString +"T"+ timeEndOfPeriod);
    		
    		while (periodStartDate.toLocalDate().isBefore(termEnd.toLocalDate())) {
    			if (periodStartDate.toLocalDate().isAfter(termStart.toLocalDate()) || periodStartDate.toLocalDate().isEqual(termStart.toLocalDate())) {
    				System.out.println("create period instance start : "+periodStartDate.toDate() +" End : "+periodEndDate.toDate());
        			SchedualStructurePeriodInstance instance = new SchedualStructurePeriodInstance();
        			instance.setStartDate(periodStartDate.toDate());
        			instance.setEndDate(periodEndDate.toDate());
        			instance.setPeriod(schedualStructurePeriod);
            		saveBaseEntity(instance);
				}
        		periodStartDate = periodStartDate.plusWeeks(1);
        		periodEndDate = periodEndDate.plusWeeks(1);
			}
    }
	
	@Override
	public void deleteSchedualStructurePeriodInstances(SchedualStructurePeriod schedualStructurePeriod) {
		List<SchedualStructurePeriod> periods = schedualStructureDao.getRelatedPeriodsInSchedual(schedualStructurePeriod);
		for (SchedualStructurePeriod period : periods) {
			deleteEntity(period);
		}
	}
	
	@Override
	public void updateSchedualStructurePeriodInstances(SchedualStructurePeriod schedualStructurePeriod) {
		SchedualStructurePeriod schedualStructurePeriodBeforeUpdate = getObjectById(schedualStructurePeriod.getId(), SchedualStructurePeriod.class);
		List<SchedualStructurePeriod> periods = schedualStructureDao.getRelatedPeriodsInSchedual(schedualStructurePeriodBeforeUpdate);
		DateTime periodStartDate = new DateTime(schedualStructurePeriod.getStartDate());
		DateTime periodEndDate = new DateTime(schedualStructurePeriod.getEndDate());
		for (SchedualStructurePeriod period : periods) {
			period.setStartDate(periodStartDate.toDate());
			period.setEndDate(periodEndDate.toDate());
			period.setName(schedualStructurePeriod.getName());
			period.setSchedualPeriodType(schedualStructurePeriod.getSchedualPeriodType());
			period.setDay(schedualStructurePeriod.getDay());
			period.setSchedualStructure(schedualStructurePeriod.getSchedualStructure());
    		saveOrUpdate(period);
    		periodStartDate = periodStartDate.plusWeeks(1);
    		periodEndDate = periodEndDate.plusWeeks(1);
		}
	}
	
	@Override
	public List<SchedualStructurePeriodInstance> getSchedualStructurePeriodInstancesClassRoom(long classRoomId, long schedualStructureID) {
		List<SchedualStructurePeriodInstance> instances = schedualStructureDao.getSchedualStructurePeriodInstancesClassRoom(classRoomId, schedualStructureID);
		for (SchedualStructurePeriodInstance instance : instances) {
			instance.setSchedualStructure(instance.getPeriod().getSchedualStructure());
			instance.setPeriodType(instance.getPeriod().getSchedualPeriodType());
			instance.setDay(instance.getPeriod().getDay());
		}
		return  instances;
	}
	
	@Override
	public List<SchedualStructurePeriodInstance> getPeriodInstances(SchedualStructurePeriod schedualStructurePeriod) {
		return schedualStructureDao.getPeriodInstances(schedualStructurePeriod);
	}
	
	@Override
	public List<SchedualStructurePeriod> getSchedualStructurePeriodsBySSID(long sSId) {
		List<SchedualStructurePeriod> schedualStructurePeriods = schedualStructureDao.getSchedualStructurePeriodsBySSID(sSId);
		for (SchedualStructurePeriod schedualStructurePeriod : schedualStructurePeriods) {
    		Days days = Days.daysBetween(new DateTime(schedualStructurePeriod.getStartDate()), LMSUtil.getTermStart());
    		Integer plusDays = 0;
    		switch (schedualStructurePeriod.getDay().toString().toUpperCase()) {
    		case "SATURDAY":
    			plusDays = 0;
    			break;
    		case "SUNDAY":
    			plusDays = 1;
    			break;
    		case "MONDAY":
    			plusDays = 2;
    			break;
    		case "TUESDAY":
    			plusDays = 3;
    			break;
    		case "WEDNESDAY":
    			plusDays = 4;
    			break;
    		case "THURSDAY":
    			plusDays = 5;
    			break;
    		case "FRIDAY":
    			plusDays = 6;
    			break;
    		}
    		Calendar startDate = Calendar.getInstance(); 
    		startDate.setTime(schedualStructurePeriod.getStartDate()); 
    		if(days.getDays() > 0)
    		startDate.add(Calendar.DATE, days.getDays() + plusDays);
    		schedualStructurePeriod.setStartDate(startDate.getTime());
    		
    		Calendar endDate = Calendar.getInstance(); 
    		endDate.setTime(schedualStructurePeriod.getEndDate()); 
    		if(days.getDays() > 0)
    		endDate.add(Calendar.DATE, days.getDays() + plusDays);
    		schedualStructurePeriod.setEndDate(endDate.getTime());
    		schedualStructureDao.getSession().evict(schedualStructurePeriod);
    		
		}
		return schedualStructurePeriods;
	}
	
	@Override
	public List<SchedualStructurePeriod> getSchedualStructurePeriodsToUser(long userId) {
		return schedualStructureDao.getSchedualStructurePeriodsToUser(userId);
	}
	
	@Override
	public void deletePreviousSchedualStructurePeriods(SchedualStructure schedualStructure) {
		schedualStructureDao.deletePreviousSchedualStructurePeriods(schedualStructure);
	}
	
	@Override
	public void createClassRoomAndRelatedContainers(ClassRoom[] classRooms) {
		schedualStructureDao.createClassRoomAndRelatedContainers(classRooms) ;
	}
	
	@Override
	public ClassRoom getPreviousClassRoomForSchedualStructurePeriodBySSPID(long sSPID) {
		return schedualStructureDao.getPreviousClassRoomForSchedualStructurePeriodBySSPID(sSPID) ;
	}
	
	public SchedualStructureDao getSchedualStructureDao() {
		return schedualStructureDao;
	}

	public void setSchedualStructureDao(SchedualStructureDao schedualStructureDao) {
		this.schedualStructureDao = schedualStructureDao;
	}
	
	@Override
	public List<SchedualPeriodType> getAllSchedualPeriodTypes() {
		return schedualStructureDao.getAllSchedualPeriodTypes();
	}
	
	@Override
	public List<SchedualStructure> getAllSchedualStructures() {
		return schedualStructureDao.getAllSchedualStructures();
	}
}
