package com.system.modules.schedualStructure.service;

import java.text.ParseException;
import java.util.List;
import com.system.modules.common.service.BaseService;
import com.system.modules.core.model.ClassRoom;
import com.system.modules.schedualStructure.model.SchedualPeriodType;
import com.system.modules.schedualStructure.model.SchedualStructure;
import com.system.modules.schedualStructure.model.SchedualStructurePeriod;
import com.system.modules.schedualStructure.model.SchedualStructurePeriodInstance;
import com.system.modules.schedualStructure.model.Section;

public interface SchedualStructureService extends BaseService {
	
	List<SchedualPeriodType> getAllSchedualPeriodTypes();

	List<SchedualStructure> getAllSchedualStructures();

	List<SchedualStructurePeriod> getSchedualStructurePeriodsBySSID(long sSId);

	void deletePreviousSchedualStructurePeriods(SchedualStructure schedualStructure);

	void createClassRoomAndRelatedContainers(ClassRoom[] classRooms);

	ClassRoom getPreviousClassRoomForSchedualStructurePeriodBySSPID(long sSPID);

	List<SchedualStructurePeriod> getSchedualStructurePeriodsToUser(long userId);

	void updateSchedualStructurePeriod(SchedualStructurePeriod schedualStructurePeriod);

	void createcheduleStructurePeriod(SchedualStructurePeriod schedualStructurePeriod);

	void deleteSchedualStructurePeriod(SchedualStructurePeriod schedualStructurePeriod);

	void deleteSchedualStructurePeriodInstances(SchedualStructurePeriod schedualStructurePeriod);

	void updateSchedualStructurePeriodInstances(SchedualStructurePeriod schedualStructurePeriod);

	void createScheduleStructurePeriodInstances(SchedualStructurePeriod schedualStructurePeriod);

	List<SchedualStructurePeriodInstance> getPeriodInstances(SchedualStructurePeriod schedualStructurePeriod);

	List<SchedualStructurePeriodInstance> getSchedualStructurePeriodInstancesClassRoom(long classRoomId, long schedualStructureID);

}
